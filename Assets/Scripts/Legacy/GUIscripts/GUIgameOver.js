var alignCenterSkin : GUISkin;
var loseLabel : GUIStyle;
var restartButton : GUIStyle;
var menuButton : GUIStyle;
var loseTexture : Texture2D;
var loseAudio : AudioClip;

private var slideIn : boolean = true;
private var menuPositionY : float = Screen.height + 200;


function Start()
{
	GetComponent.<AudioSource>().PlayOneShot (loseAudio);	
}


function Update()
{
	if (slideIn)
	{
		menuPositionY = Mathf.SmoothStep(menuPositionY, Screen.height / 2, .3);
	}
	else
	{
		menuPositionY = Mathf.SmoothStep(menuPositionY, Screen.height + 250, .3);	
	}
}



function OnGUI()
{
	GUI.depth = -200;
	GUI.skin = alignCenterSkin;
	
	GUI.DrawTexture (Rect (Screen.width / 2 - 98, menuPositionY - 190, 196, 236), loseTexture);
	
	GUI.Label (Rect (Screen.width / 2 - 128, menuPositionY - 10, 256, 64), "", loseLabel);
	
	
	if(GUI.Button (Rect (Screen.width / 2 - 90, menuPositionY + 70, 180, 50), "", restartButton)) 
	{
		LevelLoader(Application.loadedLevel, .5);
	}
	
	if(GUI.Button (Rect (Screen.width / 2 - 90, menuPositionY + 135, 180, 50), "", menuButton)) 
	{
		var menuIndex : int = 1;
		LevelLoader(menuIndex, .5);
	} 	
}



function LevelLoader(level : int, loadDelay : float)
{
	menuPositionY = Screen.height / 2;
	slideIn = false;
	yield WaitForSeconds(loadDelay);
	Application.LoadLevel(level);
}




/*
var alpha : float = 1.0;
var blendTime : float = 1.0;


function OnGUI()
{
	GUI.color.a = alpha;
	
	GUI.skin = alignCenterSkin;
	GUI.Label (Rect (Screen.width / 2 - 100, 150, 200, 100), "You Lose");
	
	
	if(GUI.Button (Rect (Screen.width / 2 - 75, Screen.height / 2 - 35, 150, 50), "Restart")) 
	{
		//BlendAlpha(.02);
		//gameGuiScript.BlendAlpha(.012);
		LevelLoader(Application.loadedLevel, .5);
	}
	
	if(GUI.Button (Rect (Screen.width / 2 - 75, Screen.height / 2 + 35, 150, 50), "Main Menu")) 
	{
		var menuIndex : int = 1;
		//BlendAlpha(.04);
		LevelLoader(menuIndex, .5);
	} 	
}
*/



/*
function BlendAlpha(step : float)
{
	minAlpha = 0;
	maxAlpha = 1;
	
	if(alpha == minAlpha)
	{
		while (alpha < maxAlpha)
		{

			alpha += step;
			yield;
		}
		alpha = maxAlpha;
	}
	else
	{
		while (alpha > minAlpha)
		{
			alpha -= step;
			yield;
		}
		alpha = minAlpha;
	}
}
*/



