/***********************************************
Order of variables in SetVariables()
0.  generatorDelay : float;
1.  numOnScreen : int;
2.  freqOfPumpkins : float;
3.  freqOfFire : float;
4.  freqOfIce : float;
5.  freqOfBomb : float;
6.  fireTimer : float;
7.  enemyHealth : int;
8.  startScore : float;  //TIME
9.  maxScore : float;
10. scoreRatio : float;
11. gravitySpeed : Vector3;
************************************************/

//                                   0   1   2   3  4  5   6   7   8    9   10  11
var easySettings : float[] =       [2.5, 6, 100, 0, 0, 0, 5.0,  6, 60, 60,   1, 4];
var hardSettings : float[] =       [1.0, 6, 100, 0, 0, 0, 4.0, 15, 30, 30, .75, 6];
var impassibleSettings : float[] = [ .5, 6, 100, 0, 0, 0, 3.0, 30, 15, 15,  .5, 8];

//private var difficultySetting : String = "Easy";
//private var difficultySetting : String = "Hard";
//private var difficultySetting : String = "Impassible";


//difficulty from playerprefs
private var difficultySetting : String = PlayerPrefs.GetString("Difficulty");

//references to scripts
private var generatorScript : blockGenerator;
private var scoreScript : scoreTracker;



function Start()
{
	generatorScript = GameObject.Find("levelScripts").GetComponent(blockGenerator);
	scoreScript = GameObject.Find("levelScripts").GetComponent(scoreTracker);
	
	var levelSettings : float[] = easySettings;
	 
	switch(difficultySetting)
	{
		case "Hard":
			levelSettings = hardSettings;
			break;
		case "Impassible":
			levelSettings = impassibleSettings;
			break;
		default:  //easy
			break;	
	}

	SetVariables(levelSettings);
}



function SetVariables (levelSettings : float[])
{
	generatorScript.delay = levelSettings[0];
	generatorScript.numberOnScreen = levelSettings[1];
	generatorScript.pumpkinFreq = levelSettings[2];
	generatorScript.fireFreq = levelSettings[3];
	generatorScript.iceFreq = levelSettings[4];
	generatorScript.bombFreq = levelSettings[5];
	generatorScript.fireTimeOut = levelSettings[6];
	scoreScript.enemyHealth = levelSettings[7];
	scoreScript.score = levelSettings[8];
	scoreScript.maxScore = levelSettings[9];
	scoreScript.scoreRatio = levelSettings[10];
	Physics.gravity.y = -levelSettings[11];

}