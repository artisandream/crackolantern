﻿using UnityEngine;
using System.Collections;

public class StaticVars {

	//this changes the state (abilities and attributes) of the pumpkins
	public enum AvaliableStates {
		Standard,
		Ice,
		Fire,
		Nuke
	}

	public static AvaliableStates currentState = AvaliableStates.Standard;
}