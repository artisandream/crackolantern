
function LevelLoader(level : int, loadDelay : float)
{
	yield WaitForSeconds(loadDelay);
	Application.LoadLevel(level);
}



