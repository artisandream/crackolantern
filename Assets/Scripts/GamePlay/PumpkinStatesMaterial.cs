﻿using UnityEngine;
using System.Collections;

public class PumpkinStatesMaterial : PumpkinStates {

	private MeshRenderer thisMaterial;
	
	public override void ChangeStage (int _i) {
		thisMaterial = GetComponent<MeshRenderer> ();
		thisMaterial.material = pumpkinController.pumpkinMaterial[_i];
	}
}
