private var fadeSpeed : float = 2.0;
private var loopSpeed : float = 0;
private var delay : float = 0;

//Styles//
var splashTexture : Texture2D;
var splashTextureHD : Texture2D;
var startBackground : GUIStyle;
var startBackgroundHD : GUIStyle;
var easyButton : GUIStyle;
var hardButton : GUIStyle;
var impassButton : GUIStyle;
var helpButton : GUIStyle;
var resumeButton : GUIStyle;
var easterEggButton : GUIStyle;
var pumpkinTexture : GUIStyle;
var muteButton : GUIStyle;
var theFontStyle : GUIStyle;
var theFontCentered : GUIStyle;
var muteOff : Texture2D;
var muteOn : Texture2D;

private var fadeScript : cameraFade;
private var pumpkinSFXscript : eeSFX;
private var fadeDone : boolean;
private var mute : boolean = false;


private var totalSmashed : int =  (PlayerPrefs.GetInt("TotalSmashed") != 0) ? (PlayerPrefs.GetInt("TotalSmashed")) : 0;
private var lastLevel : String = PlayerPrefs.GetString("Level", "");
private var lastLevelNum : String = (lastLevel != "" && PlayerPrefs.HasKey("Level")) ? (lastLevel.Replace("level", "")) : "-";
private var difficulty : String = PlayerPrefs.GetString("Difficulty", "");



function Awake()
{
	
	fadeScript = Camera.main.GetComponent(cameraFade);
	pumpkinSFXscript = GameObject.Find("pumpkinSFX").GetComponent(eeSFX);
	
	if (GameObject.Find("Audio") && GameObject.Find("Audio").GetComponent.<AudioSource>().volume == 0)
	{
		mute = true;
		muteButton.normal.background = muteOn;	
	}
}

function Start() {
	Application.PreloadLevel("level01");	
}
function Update()
{
	fadeDone = fadeScript.fadeComplete;

}


function OnGUI () {
	GUI.depth = 0;
	var theDeviceWidth = Screen.width;
	var TheWidthDifference: int;
	var theHeightDifference: int;
	var theAudioDifference: int;
	var theSmashedDifference: int;
	var theLevelDifference: int;
	if (theDeviceWidth > 700){
		GUI.Box (Rect (-128,0,1024,1024), "", startBackgroundHD);
		theWidthDifference = 18;
		theHeightDifference = 0;
		theAudioDifference = 90;
		theSmashedDifference = 82;
		theLevelDifference = 96;
		theResumeDifference = 40;
	} else {
		GUI.Box (Rect (-96,-16,512,512), "", startBackground);
		theWidthDifference = 0;
		theHeightDifference = 1;
		theAudioDifference = 48;
		theSmashedDifference = 44;
		theLevelDifference = 56;
		theResumeDifference = 0;
	}
	// Make a background box
		
//	if (GUI.Button (Rect (0, 0, 196, 236), "", pumpkinTexture))
//	{
//		if(fadeDone)
//		{
//			pumpkinSFXscript.PlayEasterEgg();
//		}	
//	}
	if (GUI.Button (Rect (theAudioDifference, Screen.height - 50, 32, 32), "", muteButton))
	{
		if (GameObject.Find("Audio") && GameObject.Find("Audio").GetComponent.<AudioSource>().isPlaying && !mute)
		{
			mute = true;
			muteButton.normal.background = muteOn;
			GameObject.Find("Audio").GetComponent.<AudioSource>().volume = 0;
		}
		else if (GameObject.Find("Audio") && GameObject.Find("Audio").GetComponent.<AudioSource>().isPlaying && mute)
		{
			mute = false;
			muteButton.normal.background = muteOff;
			GameObject.Find("Audio").GetComponent.<AudioSource>().volume = 1;	
		}			
	}
	
	if (GUI.Button (Rect (Screen.width - (192 + (theWidthDifference*21)), Screen.height - 112-(52*theHeightDifference), 180, 50), "", easyButton)) {
		if(fadeDone)
		{
			PlayerPrefs.SetString("Difficulty", "Easy");
			PlayerPrefs.SetInt("TotalSmashed", 0);
			PlayerPrefs.SetString("Level", "");
			StartGame("level01");
		}
	}
	if (GUI.Button (Rect (Screen.width - (192 + theWidthDifference*11), Screen.height - 112, 180, 50), "", hardButton)) {
		if(fadeDone)
		{
			PlayerPrefs.SetString("Difficulty", "Hard");
			PlayerPrefs.SetInt("TotalSmashed", 0);
			PlayerPrefs.SetString("Level", "");
			StartGame("level01");
		}
	}
	if (GUI.Button (Rect (Screen.width - (192 + theWidthDifference), Screen.height - 112+(52*theHeightDifference), 180, 50), "", impassButton)) {
		if(fadeDone)
		{
			PlayerPrefs.SetString("Difficulty", "Impassible");
			PlayerPrefs.SetInt("TotalSmashed", 0);
			PlayerPrefs.SetString("Level", "");
			StartGame("level01");
		}
	}
		if (GUI.Button (Rect (Screen.width - (192 + theWidthDifference), Screen.height - 234,180,32), "", helpButton)) {
		if(fadeDone)
		{
			StartGame("help");
		}
	}
	
	if (lastLevel != "" && lastLevel != "level01")
	{
		if (GUI.Button (Rect (21 + theResumeDifference, Screen.height - 100 ,88,33), "", resumeButton)) 
		{
			if(fadeDone)
			{
				StartGame(lastLevel);
			}
		}
		GUI.Label (Rect (21 + theResumeDifference,Screen.height - 124, 88, 20), difficulty, theFontCentered);
	}
	
	GUI.Label (Rect (theSmashedDifference, Screen.height -188, 52, 20), totalSmashed.ToString(), theFontStyle);
	GUI.Label (Rect (theLevelDifference, Screen.height -153, 39, 20), lastLevelNum, theFontStyle);
}



function StartGame(level : String)
{
	fadeScript.fadeComplete = false;
	fadeScript.FadeOut(0, 1, fadeSpeed, loopSpeed, delay);
	
	while (!fadeScript.fadeComplete)
	{
		yield;
	}
	yield WaitForSeconds(1);
	Application.LoadLevel(level);
}
