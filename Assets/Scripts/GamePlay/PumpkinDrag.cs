﻿using UnityEngine;
using System.Collections;

public class PumpkinDrag : MonoBehaviour {

	Rigidbody thisRigidBody;

	void Start () {
		thisRigidBody = GetComponent<Rigidbody> ();
	}

	void OnMouseDown () {
		thisRigidBody.isKinematic = true;
	}

	void OnMouseUp () {
		thisRigidBody.isKinematic = false;
	}

	public float speed = 20;
	
	void OnMouseDrag () {
		Vector3 mousePosition = 
			Camera.main.ScreenToWorldPoint (Input.mousePosition);
		mousePosition.z = 0;
		Vector3 currentPosition = 
			Vector3.Lerp (transform.position, mousePosition, speed * Time.deltaTime);
		thisRigidBody.MovePosition (currentPosition);
	}
}