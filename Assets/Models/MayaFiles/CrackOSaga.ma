//Maya ASCII 2012 scene
//Name: CrackOSaga.ma
//Last modified: Thu, Sep 22, 2011 11:13:56 AM
//Codeset: 1252
requires maya "2012";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2012";
fileInfo "version" "2012 x64";
fileInfo "cutIdentifier" "001200000000-796618";
fileInfo "osv" "Microsoft Windows 7 Home Premium Edition, 64-bit Windows 7  (Build 7600)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 6.7137582792401709 4.2100572534446297 21.085389798756445 ;
	setAttr ".r" -type "double3" -5.7383527299936237 18.199999999998262 1.0462658731508439e-016 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 21.603640435306591;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0 2.05 0.66537798465950215 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 2.201216467027574 1.3480468315030938 100.20190920302886 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 11.656958489452688;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.10906361602001 1.3404087991361195 0.13166812853624824 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 4.226714490458745;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "iPhone";
createNode transform -n "Guides_GRP" -p "iPhone";
	setAttr ".t" -type "double3" 0 0 1.9124837832968788 ;
createNode transform -n "OuterGuide" -p "Guides_GRP";
createNode nurbsCurve -n "OuterGuideShape" -p "OuterGuide";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		-1.3294999599456787 4.2460000514984131 0
		1.3294999599456787 4.2460000514984131 0
		1.3294999599456787 0.25399994850158691 0
		-1.3294999599456787 0.25399994850158691 0
		-1.3294999599456787 4.2460000514984131 0
		;
createNode transform -n "ActionGuide" -p "Guides_GRP";
createNode nurbsCurve -n "ActionGuideShape" -p "ActionGuide";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		1 4 0 no 3
		5 0 1 2 3 4
		5
		-1.25 4.125 6.3667739700137838e-016
		1.25 4.125 1.8613244084052238e-015
		1.25 0.375 -6.3667739700137838e-016
		-1.25 0.375 -1.8613244084052238e-015
		-1.25 4.125 6.3667739700137838e-016
		;
createNode transform -n "LevelBase" -p "iPhone";
createNode transform -n "Hud" -p "LevelBase";
	setAttr ".rp" -type "double3" 0 2.25 -0.15487700489403977 ;
	setAttr ".sp" -type "double3" 0 2.25 -0.15487700489403977 ;
createNode mesh -n "HudShape" -p "Hud";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.84375 0.765625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.6875 0.53124994
		 1 0.53124994 1 1 0.6875 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0.25 2.625 -0.21364856 -0.25 
		2.6250002 -0.21364856 0.25 1.8750001 -0.15487695 -0.25 1.8750001 -0.21364856;
	setAttr -s 4 ".vt[0:3]"  -1.5 -2.25 0 1.5 -2.25 0 -1.5 2.25 0 1.5 2.25 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Backgrounds" -p "LevelBase";
createNode transform -n "BackGround1" -p "Backgrounds";
	setAttr ".rp" -type "double3" 0 2.3850395230599428 -0.22502702521727203 ;
	setAttr ".sp" -type "double3" 0 2.3850395230599428 -0.22502702521727203 ;
createNode mesh -n "BackGround1Shape" -p "BackGround1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".vt[0:3]"  -2 0.38503957 -0.22502702 2 0.38503957 -0.22502702
		 -2 4.38503933 -0.22502702 2 4.38503933 -0.22502702;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "BackGround5" -p "Backgrounds";
	setAttr ".rp" -type "double3" 0 2.3850395230599428 -0.22502702521727203 ;
	setAttr ".sp" -type "double3" 0 2.3850395230599428 -0.22502702521727203 ;
createNode mesh -n "BackGround5Shape" -p "BackGround5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".vt[0:3]"  -2 0.38503957 -0.22502702 2 0.38503957 -0.22502702
		 -2 4.38503933 -0.22502702 2 4.38503933 -0.22502702;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "BackGround10" -p "Backgrounds";
	setAttr ".rp" -type "double3" 0 2.3850395230599428 -0.22502702521727203 ;
	setAttr ".sp" -type "double3" 0 2.3850395230599428 -0.22502702521727203 ;
createNode mesh -n "BackGround10Shape" -p "BackGround10";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".vt[0:3]"  -2 0.38503957 -0.22502702 2 0.38503957 -0.22502702
		 -2 4.38503933 -0.22502702 2 4.38503933 -0.22502702;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "BackGround15" -p "Backgrounds";
	setAttr ".rp" -type "double3" 0 2.3850395230599428 -0.22502702521727203 ;
	setAttr ".sp" -type "double3" 0 2.3850395230599428 -0.22502702521727203 ;
createNode mesh -n "BackGround15Shape" -p "BackGround15";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0 1 0 0 1 1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".vt[0:3]"  -2 0.38503957 -0.22502702 2 0.38503957 -0.22502702
		 -2 4.38503933 -0.22502702 2 4.38503933 -0.22502702;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 3 2;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Sound" -p "LevelBase";
createNode transform -n "Sound_Off" -p "Sound";
	setAttr ".rp" -type "double3" 1.0797209724239252 0.535 -0.040691251277406859 ;
	setAttr ".sp" -type "double3" 1.0797209724239252 0.535 -0.040691251277406859 ;
createNode mesh -n "Sound_OffShape" -p "|iPhone|LevelBase|Sound|Sound_Off";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.67112275710756097 0.79662686586380005 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.65549779 0.78100187
		 0.68674779 0.78100187 0.68674773 0.81225187 0.65549779 0.81225187;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  1.204721 0.66000003 -0.040691253 
		0.95472097 0.66000003 -0.040691253 1.204721 0.41000003 -0.040691253 0.95472097 0.41000003 
		-0.040691253;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.24999997 0
		 0.25 0.24999997 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "Sound_On" -p "Sound";
	setAttr ".rp" -type "double3" 1.0797209724239252 0.535 -0.12010141425278989 ;
	setAttr ".sp" -type "double3" 1.0797209724239252 0.535 -0.12010141425278989 ;
createNode mesh -n "Sound_OnShape" -p "|iPhone|LevelBase|Sound|Sound_On";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.64067527039488203 0.79662686586380005 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.62505031 0.78100187
		 0.65630031 0.78100187 0.65630031 0.81225187 0.62505031 0.81225187;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  1.204721 0.66000003 -0.12010141 
		0.95472097 0.66000003 -0.12010141 1.204721 0.41 -0.12010141 0.95472097 0.41 -0.12010141;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.25 0 0.25 0.25 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "Sound_Over" -p "Sound";
	setAttr ".rp" -type "double3" 1.0797209724239252 0.535 -0.083552457602300817 ;
	setAttr ".sp" -type "double3" 1.0797209724239252 0.535 -0.083552457602300817 ;
createNode mesh -n "Sound_OverShape" -p "|iPhone|LevelBase|Sound|Sound_Over";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.62505030632019043 0.78100186586380005 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.62505031 0.74975187
		 0.65630031 0.74975187 0.65630031 0.78100187 0.62505031 0.78100187;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  1.204721 0.66000003 -0.083552457 
		0.95472097 0.66000003 -0.083552457 1.204721 0.41000003 -0.083552457 0.95472097 0.41000003 
		-0.083552457;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.24999997 0
		 0.25 0.24999997 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "Pause" -p "LevelBase";
createNode transform -n "Pause_Over" -p "|iPhone|LevelBase|Pause";
	setAttr ".rp" -type "double3" -1.08 0.535 -0.083552457602300817 ;
	setAttr ".sp" -type "double3" -1.08 0.535 -0.083552457602300817 ;
createNode mesh -n "Pause_OverShape" -p "Pause_Over";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.64065194129943848 0.828125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.62502694 0.8125
		 0.65627694 0.8125 0.65627694 0.84375 0.62502694 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.95499998 0.66000003 -0.083552457 
		-1.205 0.66000003 -0.083552457 -0.95499998 0.41000003 -0.083552457 -1.205 0.41000003 
		-0.083552457;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.24999997 0
		 0.25 0.24999997 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "Pause_Norm" -p "|iPhone|LevelBase|Pause";
	setAttr ".rp" -type "double3" -1.08 0.535 -0.12010141425278989 ;
	setAttr ".sp" -type "double3" -1.08 0.535 -0.12010141425278989 ;
createNode mesh -n "Pause_NormShape" -p "Pause_Norm";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.64065194129943848 0.859375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.62502694 0.84375
		 0.65627694 0.84375 0.65627694 0.875 0.62502694 0.875;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.95499998 0.66000003 -0.12010141 
		-1.205 0.66000003 -0.12010141 -0.95499998 0.41 -0.12010141 -1.205 0.41 -0.12010141;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.25 0 0.25 0.25 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "Walls" -p "LevelBase";
createNode transform -n "Plane_LeftBrickWall" -p "Walls";
	setAttr ".rp" -type "double3" -1.25 2.25 -0.2136486802466335 ;
	setAttr ".sp" -type "double3" -1.25 2.25 -0.2136486802466335 ;
createNode mesh -n "Plane_LeftBrickWallShape" -p "Plane_LeftBrickWall";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.93747547268867493 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.49999997 0.87502563
		 0.62528336 0.87502563 0.62528318 0.99992526 0.50000006 0.99992526 0.62528324 0.99992526
		 0.50000006 0.87502575 0.62528336 0.99992532 0.50000006 0.87502575 0.6252833 0.99992526
		 0.50000006 0.87502593 0.62528312 0.99992532 0.49999997 0.87502587 0.6252833 0.99992526
		 0.50000006 0.87502581 0.62528336 0.99992532 0.49999997 0.87502587 0.62528336 0.99992526
		 0.5 0.87502587 0.62528336 0.99992526 0.50000006 0.87502599 0.6252833 0.87502587 0.5
		 0.99992532 0.62528312 0.87502581 0.49999997 0.99992526 0.6252833 0.87502587 0.50000006
		 0.99992532 0.62528336 0.87502593 0.49999997 0.99992526 0.62528324 0.87502575 0.50000006
		 0.99992532 0.62528318 0.87502575 0.50000006 0.99992526 0.62528336 0.87502599 0.49999997
		 0.99992526 0.62528336 0.87502587 0.50000006 0.99992526;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 20 ".pt[0:19]" -type "float3"  0 0 -0.21364868 0 0 -0.21364868 
		0 0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 
		0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 0 
		-0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 
		0 0 -0.21364868 0 0 -0.21364868;
	setAttr -s 20 ".vt[0:19]"  -1.5 -2.3841858e-007 0 -1 -2.3841858e-007 0
		 -1.5 4.5 0 -1 4.5 0 -1 4 0 -1.5 4 0 -1 3.5 0 -1.5 3.5 0 -1 3 0 -1.5 3 0 -1 2.5 0
		 -1.5 2.5 0 -1 2 0 -1.5 2 0 -1 1.5 0 -1.5 1.5 0 -1 0.99999988 0 -1.5 0.99999988 0
		 -1 0.49999982 0 -1.5 0.49999982 0;
	setAttr -s 28 ".ed[0:27]"  0 1 0 0 19 0 1 18 0 2 3 0 4 3 0 5 2 0 4 5 1
		 6 4 0 7 5 0 6 7 1 8 6 0 9 7 0 8 9 1 10 8 0 11 9 0 10 11 1 12 10 0 13 11 0 12 13 1
		 14 12 0 15 13 0 14 15 1 16 14 0 17 15 0 16 17 1 18 16 0 19 17 0 18 19 1;
	setAttr -s 9 ".fc[0:8]" -type "polyFaces" 
		f 4 0 2 27 -2
		mu 0 4 0 1 18 33
		f 4 -7 4 -4 -6
		mu 0 4 5 30 2 3
		f 4 -10 7 6 -9
		mu 0 4 7 28 4 31
		f 4 -13 10 9 -12
		mu 0 4 9 26 6 29
		f 4 -16 13 12 -15
		mu 0 4 11 24 8 27
		f 4 -19 16 15 -18
		mu 0 4 13 22 10 25
		f 4 -22 19 18 -21
		mu 0 4 15 20 12 23
		f 4 -25 22 21 -24
		mu 0 4 17 34 14 21
		f 4 -28 25 24 -27
		mu 0 4 19 32 16 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Plane_RightBrickWall" -p "Walls";
	setAttr ".rp" -type "double3" 1.25 2.25 -0.2136486802466335 ;
	setAttr ".sp" -type "double3" 1.25 2.25 -0.2136486802466335 ;
createNode mesh -n "Plane_RightBrickWallShape" -p "Plane_RightBrickWall";
	addAttr -ci true -sn "mso" -ln "miShadingSamplesOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "msh" -ln "miShadingSamples" -min 0 -smx 8 -at "float";
	addAttr -ci true -sn "mdo" -ln "miMaxDisplaceOverride" -min 0 -max 1 -at "bool";
	addAttr -ci true -sn "mmd" -ln "miMaxDisplace" -min 0 -smx 1 -at "float";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.56264165043830872 0.93747550249099731 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.49999988 0.99992537
		 0.62528318 0.99992502 0.62528324 0.87502575 0.49999988 0.87502563 0.62528318 0.99992502
		 0.49999988 0.87502563 0.62528318 0.99992502 0.49999988 0.87502569 0.62528318 0.99992502
		 0.49999988 0.87502569 0.62528318 0.99992502 0.49999988 0.87502569 0.62528318 0.99992502
		 0.49999988 0.87502569 0.62528318 0.99992502 0.49999988 0.87502563 0.62528318 0.99992502
		 0.49999988 0.87502569 0.62528318 0.99992502 0.49999988 0.87502569 0.62528324 0.87502575
		 0.49999988 0.99992537 0.62528324 0.87502575 0.49999988 0.99992537 0.62528324 0.87502575
		 0.49999988 0.99992537 0.62528324 0.87502575 0.49999988 0.99992537 0.62528324 0.87502575
		 0.49999988 0.99992537 0.62528324 0.87502575 0.49999988 0.99992537 0.6252833 0.87502575
		 0.49999988 0.99992537 0.62528324 0.87502575 0.49999988 0.99992537;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".op" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 20 ".pt[0:19]" -type "float3"  0 0 -0.21364868 0 0 -0.21364868 
		0 0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 
		0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 0 
		-0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 0 0 -0.21364868 
		0 0 -0.21364868 0 0 -0.21364868;
	setAttr -s 20 ".vt[0:19]"  1.5 -2.3841858e-007 0 1 -2.3841858e-007 0
		 1.5 4.5 0 1 4.5 0 1 4 0 1.5 4 0 1 3.5 0 1.5 3.5 0 1 3 0 1.5 3 0 1 2.5 0 1.5 2.5 0
		 1 2 0 1.5 2 0 1 1.5 0 1.5 1.5 0 1 0.99999988 0 1.5 0.99999988 0 1 0.49999982 0 1.5 0.49999982 0;
	setAttr -s 28 ".ed[0:27]"  0 1 0 0 19 0 1 18 0 2 3 0 4 3 0 5 2 0 4 5 1
		 6 4 0 7 5 0 6 7 1 8 6 0 9 7 0 8 9 1 10 8 0 11 9 0 10 11 1 12 10 0 13 11 0 12 13 1
		 14 12 0 15 13 0 14 15 1 16 14 0 17 15 0 16 17 1 18 16 0 19 17 0 18 19 1;
	setAttr -s 9 ".fc[0:8]" -type "polyFaces" 
		f 4 5 3 -5 6
		mu 0 4 35 3 2 4
		f 4 8 -7 -8 9
		mu 0 4 33 5 34 6
		f 4 11 -10 -11 12
		mu 0 4 31 7 32 8
		f 4 14 -13 -14 15
		mu 0 4 29 9 30 10
		f 4 17 -16 -17 18
		mu 0 4 27 11 28 12
		f 4 20 -19 -20 21
		mu 0 4 25 13 26 14
		f 4 23 -22 -23 24
		mu 0 4 23 15 24 16
		f 4 26 -25 -26 27
		mu 0 4 21 17 22 18
		f 4 1 -28 -3 -1
		mu 0 4 0 19 20 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Plane_Ground" -p "Walls";
	setAttr ".rp" -type "double3" 0 0.25 -0.18172780832914415 ;
	setAttr ".sp" -type "double3" 0 0.25 -0.18172780832914415 ;
createNode mesh -n "Plane_GroundShape" -p "Plane_Ground";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.75 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 24 ".uvst[0].uvsp[0:23]" -type "float2" 0.5 0.75 0.625 0.75
		 0.625 0.875 0.5 0.875 0.5 0.875 0.625 0.75 0.5 0.875 0.625 0.75 0.5 0.875 0.625 0.75
		 0.5 0.875 0.625 0.75 0.5 0.875 0.625 0.75 0.625 0.875 0.5 0.75 0.625 0.875 0.5 0.75
		 0.625 0.875 0.5 0.75 0.625 0.875 0.5 0.75 0.625 0.875 0.5 0.75;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 14 ".pt[0:13]" -type "float3"  0 0 -0.3208167 0 0 -0.3208167 
		0 0 -0.3208167 0 0 -0.3208167 0 0 -0.3208167 0 0 -0.3208167 0 0 -0.3208167 0 0 -0.3208167 
		0 0 -0.3208167 0 0 -0.3208167 0 0 -0.3208167 0 0 -0.3208167 0 0 -0.3208167 0 0 -0.3208167;
	setAttr -s 14 ".vt[0:13]"  -1.5 0 0.13908888 1.5 0 0.13908888 -1.5 0.5 0.13908888
		 1.5 0.5 0.13908888 1 0.5 0.13908888 1 0 0.13908888 0.5 0.5 0.13908888 0.5 0 0.13908888
		 0 0.5 0.13908888 0 0 0.13908888 -0.50000006 0.5 0.13908888 -0.50000006 0 0.13908888
		 -1 0.5 0.13908888 -1 0 0.13908888;
	setAttr -s 19 ".ed[0:18]"  0 13 0 0 2 0 1 3 0 2 12 0 4 3 0 5 1 0 4 5 1
		 6 4 0 7 5 0 6 7 1 8 6 0 9 7 0 8 9 1 10 8 0 11 9 0 10 11 1 12 10 0 13 11 0 12 13 1;
	setAttr -s 6 ".fc[0:5]" -type "polyFaces" 
		f 4 5 2 -5 6
		mu 0 4 23 1 2 4
		f 4 8 -7 -8 9
		mu 0 4 21 5 22 6
		f 4 11 -10 -11 12
		mu 0 4 19 7 20 8
		f 4 14 -13 -14 15
		mu 0 4 17 9 18 10
		f 4 17 -16 -17 18
		mu 0 4 15 11 16 12
		f 4 0 -19 -4 -2
		mu 0 4 0 13 14 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dn" yes;
createNode transform -n "MainMenu" -p "iPhone";
createNode transform -n "Main" -p "MainMenu";
	setAttr ".rp" -type "double3" 0 2.25 -0.15487700489403977 ;
	setAttr ".sp" -type "double3" 0 2.25 -0.15487700489403977 ;
createNode mesh -n "MainShape" -p "Main";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.16695893 0 0.83304107
		 0 0.83304107 1.000000119209 0.16695893 1.000000119209;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 2.25 -2.2146699 0 2.25 
		-2.2146699 0 2.25 -2.2146699 0 2.25 -2.2146699;
	setAttr -s 4 ".vt[0:3]"  -1.32949996 -1.99599993 2.059792995 1.32949996 -1.99599993 2.059792995
		 -1.32949996 1.99600029 2.059792995 1.32949996 1.99600029 2.059792995;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Buttons" -p "MainMenu";
createNode transform -n "Impass_Over" -p "|iPhone|MainMenu|Buttons";
	setAttr ".rp" -type "double3" 0.44818850039191505 0.67271946385193182 0 ;
	setAttr ".sp" -type "double3" 0.44818850039191505 0.67271946385193182 0 ;
createNode mesh -n "Impass_OverShape" -p "Impass_Over";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.26367186009883881 0.8779296875 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.35156247 0.85351557
		 0.52734375 0.85351557 0.52734375 0.90234381 0.35156247 0.90234381;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0.44818851 0.67271948 0 0.44818851 
		0.67271948 0 0.44818851 0.67271948 0 0.44818851 0.67271948 0;
	setAttr -s 4 ".vt[0:3]"  -0.69999999 -0.20000005 0 0.69999993 -0.20000005 0
		 -0.69999999 0.20000005 0 0.69999993 0.20000005 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Easy_Over" -p "|iPhone|MainMenu|Buttons";
	setAttr ".rp" -type "double3" 0.44818850039191505 1.4931323120269635 0 ;
	setAttr ".sp" -type "double3" 0.44818850039191505 1.4931323120269635 0 ;
createNode mesh -n "Easy_OverShape" -p "Easy_Over";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.087890610098838806 0.9267578125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" -2.9802322e-008 0.85351563
		 0.17578125 0.85351563 0.17578125 0.90234375 -2.9802322e-008 0.90234375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0.44818851 1.4931324 0 0.44818851 
		1.4931324 0 0.44818851 1.4931324 0 0.44818851 1.4931324 0;
	setAttr -s 4 ".vt[0:3]"  -0.69999999 -0.20000005 0 0.69999993 -0.20000005 0
		 -0.69999999 0.20000005 0 0.69999993 0.20000005 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Hard_Over" -p "|iPhone|MainMenu|Buttons";
	setAttr ".rp" -type "double3" 0.44818850039191505 1.0796624253306719 0 ;
	setAttr ".sp" -type "double3" 0.44818850039191505 1.0796624253306719 0 ;
createNode mesh -n "Hard_OverShape" -p "Hard_Over";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.263671875 0.9267578125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.17578124 0.85351563
		 0.3515625 0.85351563 0.3515625 0.90234375 0.17578124 0.90234375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0.44818851 1.0796624 0 0.44818851 
		1.0796624 0 0.44818851 1.0796624 0 0.44818851 1.0796624 0;
	setAttr -s 4 ".vt[0:3]"  -0.69999999 -0.20000005 0 0.69999993 -0.20000005 0
		 -0.69999999 0.20000005 0 0.69999993 0.20000005 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Easy_Norm" -p "|iPhone|MainMenu|Buttons";
	setAttr ".rp" -type "double3" 0.44818850039191505 1.4931323120269635 0 ;
	setAttr ".sp" -type "double3" 0.44818850039191505 1.4931323120269635 0 ;
createNode mesh -n "Easy_NormShape" -p "Easy_Norm";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.087890610098838806 0.9267578125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" -2.9802322e-008 0.90234375
		 0.17578125 0.90234375 0.17578125 0.95117188 -2.9802322e-008 0.95117188;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0.44818851 1.4931324 0 0.44818851 
		1.4931324 0 0.44818851 1.4931324 0 0.44818851 1.4931324 0;
	setAttr -s 4 ".vt[0:3]"  -0.69999999 -0.20000005 0 0.69999993 -0.20000005 0
		 -0.69999999 0.20000005 0 0.69999993 0.20000005 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Hard_Norm" -p "|iPhone|MainMenu|Buttons";
	setAttr ".rp" -type "double3" 0.44818850039191505 1.0796624253306719 0 ;
	setAttr ".sp" -type "double3" 0.44818850039191505 1.0796624253306719 0 ;
createNode mesh -n "Hard_NormShape" -p "Hard_Norm";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.263671875 0.9267578125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.17578124 0.90234375
		 0.3515625 0.90234375 0.3515625 0.95117188 0.17578124 0.95117188;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0.44818851 1.0796624 0 0.44818851 
		1.0796624 0 0.44818851 1.0796624 0 0.44818851 1.0796624 0;
	setAttr -s 4 ".vt[0:3]"  -0.69999999 -0.20000005 0 0.69999993 -0.20000005 0
		 -0.69999999 0.20000005 0 0.69999993 0.20000005 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Impass_Norm" -p "|iPhone|MainMenu|Buttons";
	setAttr ".rp" -type "double3" 0.44818850039191505 0.67271946385193182 0 ;
	setAttr ".sp" -type "double3" 0.44818850039191505 0.67271946385193182 0 ;
createNode mesh -n "Impass_NormShape" -p "Impass_Norm";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.439453125 0.9267578125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.35156247 0.90234369
		 0.52734375 0.90234369 0.52734375 0.95117193 0.35156247 0.95117193;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0.44818851 0.67271948 0 0.44818851 
		0.67271948 0 0.44818851 0.67271948 0 0.44818851 0.67271948 0;
	setAttr -s 4 ".vt[0:3]"  -0.69999999 -0.20000005 0 0.69999993 -0.20000005 0
		 -0.69999999 0.20000005 0 0.69999993 0.20000005 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Resume_Norm" -p "|iPhone|MainMenu|Buttons";
	setAttr ".rp" -type "double3" -0.74757651485292964 0.9918899455214909 0 ;
	setAttr ".sp" -type "double3" -0.74757651485292964 0.9918899455214909 0 ;
createNode mesh -n "Resume_NormShape" -p "|iPhone|MainMenu|Buttons|Resume_Norm";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.2187499925494194 0.69140625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.17578124 0.69140625
		 0.26171875 0.69140625 0.26171875 0.72363287 0.17578124 0.72363287;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.38355294 1.0851479 0 -1.1116 
		1.0851479 0 -0.38355294 0.89863205 0 -1.1116 0.89863205 0;
	setAttr -s 4 ".vt[0:3]"  -0.69999999 -0.20000005 0 0.69999993 -0.20000005 0
		 -0.69999999 0.20000005 0 0.69999993 0.20000005 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Resume_Over" -p "|iPhone|MainMenu|Buttons";
	setAttr ".rp" -type "double3" -0.74757651485292964 0.9918899455214909 0 ;
	setAttr ".sp" -type "double3" -0.74757651485292964 0.9918899455214909 0 ;
createNode mesh -n "Resume_OverShape" -p "|iPhone|MainMenu|Buttons|Resume_Over";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.2187499925494194 0.75589775924813263 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.17578124 0.72412109
		 0.26171875 0.72412109 0.26171875 0.75589776 0.17578124 0.75589776;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.38355294 1.0851479 0 -1.1116 
		1.0851479 0 -0.38355294 0.89863205 0 -1.1116 0.89863205 0;
	setAttr -s 4 ".vt[0:3]"  -0.69999999 -0.20000005 0 0.69999993 -0.20000005 0
		 -0.69999999 0.20000005 0 0.69999993 0.20000005 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Instruct_Over" -p "|iPhone|MainMenu|Buttons";
	setAttr ".rp" -type "double3" 0.44818850039191505 1.9663010633476432 0 ;
	setAttr ".sp" -type "double3" 0.44818850039191505 1.9663010633476432 0 ;
createNode mesh -n "Instruct_OverShape" -p "Instruct_Over";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" -2.9802322387695313e-008 0.755859375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" -2.9802322e-008 0.72363281
		 0.17578125 0.72363281 0.17578125 0.75585938 -2.9802322e-008 0.75585938;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0.44818851 2.166301 0 0.44818851 
		2.166301 0 0.44818851 2.0080981 0 0.44818851 2.0080981 0;
	setAttr -s 4 ".vt[0:3]"  -0.69999999 -0.20000005 0 0.69999993 -0.20000005 0
		 -0.69999999 0.20000005 0 0.69999993 0.20000005 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Instruct_Norm" -p "|iPhone|MainMenu|Buttons";
	setAttr ".rp" -type "double3" 0.44818850039191505 1.9663010633476432 0 ;
	setAttr ".sp" -type "double3" 0.44818850039191505 1.9663010633476432 0 ;
createNode mesh -n "Instruct_NormShape" -p "Instruct_Norm";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" -2.9802322387695313e-008 0.7236328125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" -2.9802322e-008 0.69140625
		 0.17578125 0.69140625 0.17578125 0.72363281 -2.9802322e-008 0.72363281;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0.44818851 2.166301 0 0.44818851 
		2.166301 0 0.44818851 2.0080981 0 0.44818851 2.0080981 0;
	setAttr -s 4 ".vt[0:3]"  -0.69999999 -0.20000005 0 0.69999993 -0.20000005 0
		 -0.69999999 0.20000005 0 0.69999993 0.20000005 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Sound1" -p "MainMenu";
createNode transform -n "Sound_Off" -p "Sound1";
	setAttr ".rp" -type "double3" -0.75155161871939757 0.66119012147404077 0 ;
	setAttr ".sp" -type "double3" -0.75155161871939757 0.66119012147404077 0 ;
createNode mesh -n "Sound_OffShape" -p "|iPhone|MainMenu|Sound1|Sound_Off";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.67112275710756097 0.79662686586380005 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.65549779 0.78100187
		 0.68674779 0.78100187 0.68674773 0.81225187 0.65549779 0.81225187;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.62655163 0.78619009 0 
		-0.87655163 0.78619009 0 -0.62655163 0.53619015 0 -0.87655163 0.53619015 0;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.24999997 0
		 0.25 0.24999997 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Sound_On" -p "Sound1";
	setAttr ".rp" -type "double3" -0.75155161871939757 0.66119012147404077 0 ;
	setAttr ".sp" -type "double3" -0.75155161871939757 0.66119012147404077 0 ;
createNode mesh -n "Sound_OnShape" -p "|iPhone|MainMenu|Sound1|Sound_On";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.64067527039488203 0.79662686586380005 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.62505031 0.78100187
		 0.65630031 0.78100187 0.65630031 0.81225187 0.62505031 0.81225187;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.62655163 0.78619009 0 
		-0.87655163 0.78619009 0 -0.62655163 0.53619009 0 -0.87655163 0.53619009 0;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.25 0 0.25 0.25 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Sound_Over" -p "Sound1";
	setAttr ".rp" -type "double3" -0.75155161871939757 0.66119012147404077 1.3877787807814457e-017 ;
	setAttr ".sp" -type "double3" -0.75155161871939757 0.66119012147404077 1.3877787807814457e-017 ;
createNode mesh -n "Sound_OverShape" -p "|iPhone|MainMenu|Sound1|Sound_Over";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.62505030632019043 0.78100186586380005 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.62505031 0.74975187
		 0.65630031 0.74975187 0.65630031 0.78100187 0.62505031 0.78100187;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.62655163 0.78619009 1.3877788e-017 
		-0.87655163 0.78619009 1.3877788e-017 -0.62655163 0.53619015 1.3877788e-017 -0.87655163 
		0.53619015 1.3877788e-017;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.24999997 0
		 0.25 0.24999997 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Numbers" -p "MainMenu";
createNode transform -n "Num0" -p "Numbers";
	setAttr ".rp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
	setAttr ".sp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
createNode mesh -n "Num0Shape" -p "Num0";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.92041015625 0.9697265625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.84179688 0.94921875
		 0.87304688 0.94921875 0.87304688 0.99023438 0.84179688 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.3642019 1.9003621 2.7755576e-017 
		-0.71517265 1.9003621 2.7755576e-017 -0.3642019 1.5962766 2.7755576e-017 -0.71517265 
		1.5962766 2.7755576e-017;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.24999997 0
		 0.25 0.24999997 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Num1" -p "Numbers";
	setAttr ".rp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
	setAttr ".sp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
createNode mesh -n "Num1Shape" -p "Num1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.873046875 0.990234375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.87304688 0.94921875
		 0.90429688 0.94921875 0.90429688 0.99023438 0.87304688 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.3642019 1.9003621 2.7755576e-017 
		-0.71517265 1.9003621 2.7755576e-017 -0.3642019 1.5962766 2.7755576e-017 -0.71517265 
		1.5962766 2.7755576e-017;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.24999997 0
		 0.25 0.24999997 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Num2" -p "Numbers";
	setAttr ".rp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
	setAttr ".sp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
createNode mesh -n "Num2Shape" -p "Num2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.9208984375 0.9697265625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.90527344 0.94921875
		 0.93652344 0.94921875 0.93652344 0.99023438 0.90527344 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.3642019 1.9003621 2.7755576e-017 
		-0.71517265 1.9003621 2.7755576e-017 -0.3642019 1.5962766 2.7755576e-017 -0.71517265 
		1.5962766 2.7755576e-017;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.24999997 0
		 0.25 0.24999997 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Num3" -p "Numbers";
	setAttr ".rp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
	setAttr ".sp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
createNode mesh -n "Num3Shape" -p "Num3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.953125 0.9697265625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.9375 0.94921875
		 0.96875 0.94921875 0.96875 0.99023438 0.9375 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.3642019 1.9003621 2.7755576e-017 
		-0.71517265 1.9003621 2.7755576e-017 -0.3642019 1.5962766 2.7755576e-017 -0.71517265 
		1.5962766 2.7755576e-017;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.24999997 0
		 0.25 0.24999997 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Num4" -p "Numbers";
	setAttr ".rp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
	setAttr ".sp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
createNode mesh -n "Num4Shape" -p "Num4";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.9833984375 0.9697265625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.96777344 0.94921875
		 0.99902344 0.94921875 0.99902344 0.99023438 0.96777344 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.3642019 1.9003621 2.7755576e-017 
		-0.71517265 1.9003621 2.7755576e-017 -0.3642019 1.5962766 2.7755576e-017 -0.71517265 
		1.5962766 2.7755576e-017;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.24999997 0
		 0.25 0.24999997 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Num5" -p "Numbers";
	setAttr ".rp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
	setAttr ".sp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
createNode mesh -n "Num5Shape" -p "Num5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.8583984375 0.9072265625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.84277344 0.88671875
		 0.87402344 0.88671875 0.87402344 0.92773438 0.84277344 0.92773438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.3642019 1.9003621 2.7755576e-017 
		-0.71517265 1.9003621 2.7755576e-017 -0.3642019 1.5962766 2.7755576e-017 -0.71517265 
		1.5962766 2.7755576e-017;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.24999997 0
		 0.25 0.24999997 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Num6" -p "Numbers";
	setAttr ".rp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
	setAttr ".sp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
createNode mesh -n "Num6Shape" -p "Num6";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.8896484375 0.9072265625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.87402344 0.88671875
		 0.90527344 0.88671875 0.90527344 0.92773438 0.87402344 0.92773438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.3642019 1.9003621 2.7755576e-017 
		-0.71517265 1.9003621 2.7755576e-017 -0.3642019 1.5962766 2.7755576e-017 -0.71517265 
		1.5962766 2.7755576e-017;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.24999997 0
		 0.25 0.24999997 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Num7" -p "Numbers";
	setAttr ".rp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
	setAttr ".sp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
createNode mesh -n "Num7Shape" -p "Num7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.9208984375 0.9072265625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.90527344 0.88671875
		 0.93652344 0.88671875 0.93652344 0.92773438 0.90527344 0.92773438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.3642019 1.9003621 2.7755576e-017 
		-0.71517265 1.9003621 2.7755576e-017 -0.3642019 1.5962766 2.7755576e-017 -0.71517265 
		1.5962766 2.7755576e-017;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.24999997 0
		 0.25 0.24999997 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Num8" -p "Numbers";
	setAttr ".rp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
	setAttr ".sp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
createNode mesh -n "Num8Shape" -p "Num8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.9521484375 0.9072265625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.93652344 0.88671875
		 0.96777344 0.88671875 0.96777344 0.92773438 0.93652344 0.92773438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.3642019 1.9003621 2.7755576e-017 
		-0.71517265 1.9003621 2.7755576e-017 -0.3642019 1.5962766 2.7755576e-017 -0.71517265 
		1.5962766 2.7755576e-017;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.24999997 0
		 0.25 0.24999997 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Num9" -p "Numbers";
	setAttr ".rp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
	setAttr ".sp" -type "double3" -0.53968728244353592 1.7483193912866768 2.7755575615628914e-017 ;
createNode mesh -n "Num9Shape" -p "Num9";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.9833984375 0.9072265625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.96777344 0.88671875
		 0.99902344 0.88671875 0.99902344 0.92773438 0.96777344 0.92773438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  -0.3642019 1.9003621 2.7755576e-017 
		-0.71517265 1.9003621 2.7755576e-017 -0.3642019 1.5962766 2.7755576e-017 -0.71517265 
		1.5962766 2.7755576e-017;
	setAttr -s 4 ".vt[0:3]"  -0.25 -0.25 0 0.25 -0.25 0 -0.25 0.24999997 0
		 0.25 0.24999997 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "PauseMenu" -p "iPhone";
createNode transform -n "Pause" -p "PauseMenu";
	setAttr ".rp" -type "double3" 0 2.25 -0.15487700489403972 ;
	setAttr ".sp" -type "double3" 0 2.25 -0.15487700489403972 ;
createNode mesh -n "PauseShape" -p "|iPhone|PauseMenu|Pause";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.16695893 0 0.83304107
		 0 0.83304107 1.000000119209 0.16695893 1.000000119209;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 2.25 -2.2146699 0 2.25 
		-2.2146699 0 2.25 -2.2146699 0 2.25 -2.2146699;
	setAttr -s 4 ".vt[0:3]"  -1.32949996 -1.99599993 2.059792995 1.32949996 -1.99599993 2.059792995
		 -1.32949996 1.99600029 2.059792995 1.32949996 1.99600029 2.059792995;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Buttons" -p "PauseMenu";
createNode transform -n "Resume_Over" -p "|iPhone|PauseMenu|Buttons";
	setAttr ".rp" -type "double3" -0.69999998807907104 0.8796623776469561 -0.077338162999582183 ;
	setAttr ".sp" -type "double3" -0.69999998807907104 0.8796623776469561 -0.077338162999582183 ;
createNode mesh -n "Resume_OverShape" -p "|iPhone|PauseMenu|Buttons|Resume_Over";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.087890610098838806 0.853515625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" -2.9802322e-008 0.80444312
		 0.17578125 0.80444312 0.17578125 0.85351563 -2.9802322e-008 0.85351563;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 1.4931324 -0.077338167 
		0 1.4931324 -0.077338167 0 1.4931322 -0.077338167 0 1.4931322 -0.077338167;
	setAttr -s 4 ".vt[0:3]"  -0.69999999 -0.20000005 0 0.69999993 -0.20000005 0
		 -0.69999999 0.20000005 0 0.69999993 0.20000005 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Resume_Norm" -p "|iPhone|PauseMenu|Buttons";
	setAttr ".rp" -type "double3" -0.69999998807907104 0.8796623776469561 -0.077338162999582183 ;
	setAttr ".sp" -type "double3" -0.69999998807907104 0.8796623776469561 -0.077338162999582183 ;
createNode mesh -n "Resume_NormShape" -p "|iPhone|PauseMenu|Buttons|Resume_Norm";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.087890610098838806 0.755859375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" -2.9802322e-008 0.75585938
		 0.17578125 0.75585938 0.17578125 0.80444312 -2.9802322e-008 0.80444312;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 1.4931324 -0.077338167 
		0 1.4931324 -0.077338167 0 1.4931322 -0.077338167 0 1.4931322 -0.077338167;
	setAttr -s 4 ".vt[0:3]"  -0.69999999 -0.20000005 0 0.69999993 -0.20000005 0
		 -0.69999999 0.20000005 0 0.69999993 0.20000005 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Main_Over" -p "|iPhone|PauseMenu|Buttons";
	setAttr ".rp" -type "double3" -0.69999998807907104 0.8796623776469561 -0.077338162999582183 ;
	setAttr ".sp" -type "double3" -0.69999998807907104 0.8796623776469561 -0.077338162999582183 ;
createNode mesh -n "Main_OverShape" -p "Main_Over";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" -2.9802322387695313e-008 0.755859375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" -2.9802322e-008 0.72363281
		 0.17578125 0.72363281 0.17578125 0.75585938 -2.9802322e-008 0.75585938;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 1.0796624 -0.077338167 
		0 1.0796624 -0.077338167 0 1.0796624 -0.077338167 0 1.0796624 -0.077338167;
	setAttr -s 4 ".vt[0:3]"  -0.69999999 -0.20000005 0 0.69999993 -0.20000005 0
		 -0.69999999 0.20000005 0 0.69999993 0.20000005 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Main_Norm" -p "|iPhone|PauseMenu|Buttons";
	setAttr ".rp" -type "double3" -0.69999998807907104 0.8796623776469561 -0.077338162999582183 ;
	setAttr ".sp" -type "double3" -0.69999998807907104 0.8796623776469561 -0.077338162999582183 ;
createNode mesh -n "Main_NormShape" -p "Main_Norm";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" -2.9802322387695313e-008 0.7236328125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" -2.9802322e-008 0.69140625
		 0.17578125 0.69140625 0.17578125 0.72363281 -2.9802322e-008 0.72363281;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".bnr" 0;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 1.0796624 -0.077338167 
		0 1.0796624 -0.077338167 0 1.0796624 -0.077338167 0 1.0796624 -0.077338167;
	setAttr -s 4 ".vt[0:3]"  -0.69999999 -0.20000005 0 0.69999993 -0.20000005 0
		 -0.69999999 0.20000005 0 0.69999993 0.20000005 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "LevelsMenu" -p "iPhone";
createNode transform -n "Levels" -p "LevelsMenu";
	setAttr ".rp" -type "double3" 0 2.25 0.033509309734182313 ;
	setAttr ".sp" -type "double3" 0 2.25 0.033509309734182313 ;
createNode mesh -n "LevelsShape" -p "Levels";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.16695893 0 0.83304107
		 0 0.83304107 1.000000119209 0.16695893 1.000000119209;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 2.25 -2.0262837 0 2.25 
		-2.0262837 0 2.25 -2.0262837 0 2.25 -2.0262837;
	setAttr -s 4 ".vt[0:3]"  -1.32949996 -1.99599993 2.059792995 1.32949996 -1.99599993 2.059792995
		 -1.32949996 1.99600029 2.059792995 1.32949996 1.99600029 2.059792995;
	setAttr -s 4 ".ed[0:3]"  0 1 0 0 2 0 1 3 0 2 3 0;
	setAttr ".fc[0]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Level_Buttons" -p "LevelsMenu";
	setAttr ".t" -type "double3" 0 0 0.18838631462822159 ;
	setAttr ".rp" -type "double3" -0.1507303597833759 1.3461781416613818 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" -0.1507303597833759 1.3461781416613818 -0.083552457392215729 ;
createNode transform -n "Lev01" -p "Level_Buttons";
	setAttr ".rp" -type "double3" -0.87337574573962962 1.9999999999999996 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" -0.87337574573962962 1.9999999999999996 -0.083552457392215729 ;
createNode mesh -n "Lev0Shape1" -p "Lev01";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.84179688 0.94921875
		 0.87304688 0.94921875 0.87304688 0.99023438 0.84179688 0.99023438 0.87304688 0.94921875
		 0.90429688 0.94921875 0.90429688 0.99023438 0.87304688 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -0.94789034 -0.048978746 
		1.8626451e-009 -0.87337571 -0.048978746 1.8626451e-009 -0.94789034 0.048978567 1.8626451e-009 
		-0.87337571 0.048978567 1.8626451e-009 -0.87337571 -0.048978746 1.8626451e-009 -0.79886109 
		-0.048978746 1.8626451e-009 -0.87337571 0.048978567 1.8626451e-009 -0.79886109 0.048978567 
		1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  -0.14902927 1.90204275 -0.083552457 0 1.90204275 -0.083552457
		 -0.14902927 2.097957373 -0.083552457 0 2.097957373 -0.083552457 0 1.90204275 -0.083552457
		 0.14902927 1.90204275 -0.083552457 0 2.097957373 -0.083552457 0.14902927 2.097957373 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev02" -p "Level_Buttons";
	setAttr ".rp" -type "double3" -0.64983186336963517 1.9999999999999991 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" -0.64983186336963517 1.9999999999999996 -0.083552457392215729 ;
createNode mesh -n "Lev0Shape2" -p "Lev02";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.90527344 0.94921875
		 0.93652344 0.94921875 0.93652344 0.99023438 0.90527344 0.99023438 0.84179688 0.94921875
		 0.87304688 0.94921875 0.87304688 0.99023438 0.84179688 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -2.3916121 0.14693591 1.8626451e-009 
		-2.3170977 0.14693591 1.8626451e-009 -2.3916121 0.24489316 1.8626451e-009 -2.3170977 
		0.24489316 1.8626451e-009 -2.4661269 0.14693591 1.8626451e-009 -2.3916121 0.14693591 
		1.8626451e-009 -2.4661269 0.24489316 1.8626451e-009 -2.3916121 0.24489316 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  2 1.70612824 -0.083552457 2.14902925 1.70612824 -0.083552457
		 2 1.90204275 -0.083552457 2.14902925 1.90204275 -0.083552457 1.85097075 1.70612824 -0.083552457
		 2 1.70612824 -0.083552457 1.85097075 1.90204275 -0.083552457 2 1.90204275 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev03" -p "Level_Buttons";
	setAttr ".rp" -type "double3" -0.16806826528155128 1.9999999999999996 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" -0.16806826528155128 1.9999999999999996 -0.083552457392215729 ;
createNode mesh -n "Lev0Shape3" -p "Lev03";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.9375 0.94921875
		 0.96875 0.94921875 0.96875 0.99023438 0.9375 0.99023438 0.84179688 0.94921875 0.87304688
		 0.94921875 0.87304688 0.99023438 0.84179688 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.9098487 0.24489328 1.8626451e-009 
		-1.8353341 0.24489328 1.8626451e-009 -1.9098487 0.34285054 1.8626451e-009 -1.8353341 
		0.34285054 1.8626451e-009 -1.9843633 0.24489328 1.8626451e-009 -1.9098487 0.24489328 
		1.8626451e-009 -1.9843633 0.34285054 1.8626451e-009 -1.9098487 0.34285054 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  2 1.60817087 -0.083552457 2.14902925 1.60817087 -0.083552457
		 2 1.80408537 -0.083552457 2.14902925 1.80408537 -0.083552457 1.85097075 1.60817087 -0.083552457
		 2 1.60817087 -0.083552457 1.85097075 1.80408537 -0.083552457 2 1.80408537 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev04" -p "Level_Buttons";
	setAttr ".rp" -type "double3" 0.31369533280653172 1.9999999999999996 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" 0.31369533280653172 1.9999999999999996 -0.083552457392215729 ;
createNode mesh -n "Lev0Shape4" -p "Lev04";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.96777344 0.94921875
		 0.99902344 0.94921875 0.99902344 0.99023438 0.96777344 0.99023438 0.84179688 0.94921875
		 0.87304688 0.94921875 0.87304688 0.99023438 0.84179688 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.4280849 0.34285054 1.8626451e-009 
		-1.3535702 0.34285054 1.8626451e-009 -1.4280849 0.44080779 1.8626451e-009 -1.3535702 
		0.44080779 1.8626451e-009 -1.5025995 0.34285054 1.8626451e-009 -1.4280849 0.34285054 
		1.8626451e-009 -1.5025995 0.44080779 1.8626451e-009 -1.4280849 0.44080779 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  2 1.51021361 -0.083552457 2.14902925 1.51021361 -0.083552457
		 2 1.70612812 -0.083552457 2.14902925 1.70612812 -0.083552457 1.85097075 1.51021361 -0.083552457
		 2 1.51021361 -0.083552457 1.85097075 1.70612812 -0.083552457 2 1.70612812 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev05" -p "Level_Buttons";
	setAttr ".rp" -type "double3" -0.87337574573962984 1.8530640304088584 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" -0.87337574573962984 1.8530640304088584 -0.083552457392215729 ;
createNode mesh -n "Lev0Shape5" -p "Lev05";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.84277344 0.88671875
		 0.87402344 0.88671875 0.87402344 0.92773438 0.84277344 0.92773438 0.84179688 0.94921875
		 0.87304688 0.94921875 0.87304688 0.99023438 0.84179688 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -2.8733759 0.11389676 1.8626451e-009 
		-2.7988613 0.11389676 1.8626451e-009 -2.8733759 0.21185401 1.8626451e-009 -2.7988613 
		0.21185401 1.8626451e-009 -2.9478905 0.11389676 1.8626451e-009 -2.8733759 0.11389676 
		1.8626451e-009 -2.9478905 0.21185401 1.8626451e-009 -2.8733759 0.21185401 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  2 1.41225636 -0.083552457 2.14902925 1.41225636 -0.083552457
		 2 1.60817087 -0.083552457 2.14902925 1.60817087 -0.083552457 1.85097075 1.41225636 -0.083552457
		 2 1.41225636 -0.083552457 1.85097075 1.60817087 -0.083552457 2 1.60817087 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev06" -p "Level_Buttons";
	setAttr ".rp" -type "double3" -0.64983186336963428 1.8530640304088584 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" -0.64983186336963428 1.8530640304088584 -0.083552457392215729 ;
createNode mesh -n "Lev0Shape6" -p "Lev06";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.87402344 0.88671875
		 0.90527344 0.88671875 0.90527344 0.92773438 0.87402344 0.92773438 0.84179688 0.94921875
		 0.87304688 0.94921875 0.87304688 0.99023438 0.84179688 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -2.3916121 0.21185401 1.8626451e-009 
		-2.3170977 0.21185401 1.8626451e-009 -2.3916121 0.30981126 1.8626451e-009 -2.3170977 
		0.30981126 1.8626451e-009 -2.4661269 0.21185401 1.8626451e-009 -2.3916121 0.21185401 
		1.8626451e-009 -2.4661269 0.30981126 1.8626451e-009 -2.3916121 0.30981126 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  2 1.31429911 -0.083552457 2.14902925 1.31429911 -0.083552457
		 2 1.51021361 -0.083552457 2.14902925 1.51021361 -0.083552457 1.85097075 1.31429911 -0.083552457
		 2 1.31429911 -0.083552457 1.85097075 1.51021361 -0.083552457 2 1.51021361 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev07" -p "Level_Buttons";
	setAttr ".rp" -type "double3" -0.16806826528155128 1.8530640304088584 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" -0.16806826528155128 1.8530640304088584 -0.083552457392215729 ;
createNode mesh -n "Lev0Shape7" -p "Lev07";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.90527344 0.88671875
		 0.93652344 0.88671875 0.93652344 0.92773438 0.90527344 0.92773438 0.84179688 0.94921875
		 0.87304688 0.94921875 0.87304688 0.99023438 0.84179688 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.9098487 0.30981126 1.8626451e-009 
		-1.8353341 0.30981126 1.8626451e-009 -1.9098487 0.40776852 1.8626451e-009 -1.8353341 
		0.40776852 1.8626451e-009 -1.9843633 0.30981126 1.8626451e-009 -1.9098487 0.30981126 
		1.8626451e-009 -1.9843633 0.40776852 1.8626451e-009 -1.9098487 0.40776852 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  2 1.21634185 -0.083552457 2.14902925 1.21634185 -0.083552457
		 2 1.41225636 -0.083552457 2.14902925 1.41225636 -0.083552457 1.85097075 1.21634185 -0.083552457
		 2 1.21634185 -0.083552457 1.85097075 1.41225636 -0.083552457 2 1.41225636 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev08" -p "Level_Buttons";
	setAttr ".rp" -type "double3" 0.31369533280653172 1.8530640304088588 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" 0.31369533280653172 1.8530640304088588 -0.083552457392215729 ;
createNode mesh -n "Lev0Shape8" -p "Lev08";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.93652344 0.88671875
		 0.96777344 0.88671875 0.96777344 0.92773438 0.93652344 0.92773438 0.84179688 0.94921875
		 0.87304688 0.94921875 0.87304688 0.99023438 0.84179688 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.4280849 0.40776852 1.8626451e-009 
		-1.3535702 0.40776852 1.8626451e-009 -1.4280849 0.50572574 1.8626451e-009 -1.3535702 
		0.50572574 1.8626451e-009 -1.5025995 0.40776852 1.8626451e-009 -1.4280849 0.40776852 
		1.8626451e-009 -1.5025995 0.50572574 1.8626451e-009 -1.4280849 0.50572574 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  2 1.1183846 -0.083552457 2.14902925 1.1183846 -0.083552457
		 2 1.31429911 -0.083552457 2.14902925 1.31429911 -0.083552457 1.85097075 1.1183846 -0.083552457
		 2 1.1183846 -0.083552457 1.85097075 1.31429911 -0.083552457 2 1.31429911 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev09" -p "Level_Buttons";
	setAttr ".rp" -type "double3" -0.87337574573962928 1.5261531068274867 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" -0.87337574573962928 1.5261531068274867 -0.083552457392215729 ;
createNode mesh -n "Lev0Shape9" -p "Lev09";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.96777344 0.88671875
		 0.99902344 0.88671875 0.99902344 0.92773438 0.96777344 0.92773438 0.84179688 0.94921875
		 0.87304688 0.94921875 0.87304688 0.99023438 0.84179688 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -2.8733759 0.17881496 1.8626451e-009 
		-2.7988613 0.17881496 1.8626451e-009 -2.8733759 0.2767722 1.8626451e-009 -2.7988613 
		0.2767722 1.8626451e-009 -2.9478905 0.17881496 1.8626451e-009 -2.8733759 0.17881496 
		1.8626451e-009 -2.9478905 0.2767722 1.8626451e-009 -2.8733759 0.2767722 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  2 1.020427227 -0.083552457 2.14902925 1.020427227 -0.083552457
		 2 1.21634173 -0.083552457 2.14902925 1.21634173 -0.083552457 1.85097075 1.020427227 -0.083552457
		 2 1.020427227 -0.083552457 1.85097075 1.21634173 -0.083552457 2 1.21634173 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev10" -p "Level_Buttons";
	setAttr ".rp" -type "double3" -0.64983186336963517 1.5261531068274863 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" -0.64983186336963517 1.5261531068274863 -0.083552457392215729 ;
createNode mesh -n "LevShape10" -p "Lev10";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.84179688 0.94921875
		 0.87304688 0.94921875 0.87304688 0.99023438 0.84179688 0.99023438 0.87304688 0.94921875
		 0.90429688 0.94921875 0.90429688 0.99023438 0.87304688 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -2.3916121 0.2767722 1.8626451e-009 
		-2.3170977 0.2767722 1.8626451e-009 -2.3916121 0.37472945 1.8626451e-009 -2.3170977 
		0.37472945 1.8626451e-009 -2.4661269 0.2767722 1.8626451e-009 -2.3916121 0.2767722 
		1.8626451e-009 -2.4661269 0.37472945 1.8626451e-009 -2.3916121 0.37472945 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  2 0.92246997 -0.083552457 2.14902925 0.92246997 -0.083552457
		 2 1.11838448 -0.083552457 2.14902925 1.11838448 -0.083552457 1.85097075 0.92246997 -0.083552457
		 2 0.92246997 -0.083552457 1.85097075 1.11838448 -0.083552457 2 1.11838448 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev11" -p "Level_Buttons";
	setAttr ".rp" -type "double3" -0.16806826528155128 1.5261531068274863 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" -0.16806826528155128 1.5261531068274863 -0.083552457392215729 ;
createNode mesh -n "LevShape11" -p "Lev11";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.87304688 0.94921875
		 0.90429688 0.94921875 0.90429688 0.99023438 0.87304688 0.99023438 0.87304688 0.94921875
		 0.90429688 0.94921875 0.90429688 0.99023438 0.87304688 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.9843633 0.37472945 1.8626451e-009 
		-1.9098487 0.37472945 1.8626451e-009 -1.9843633 0.47268671 1.8626451e-009 -1.9098487 
		0.47268671 1.8626451e-009 -1.9098487 0.37472945 1.8626451e-009 -1.8353341 0.37472945 
		1.8626451e-009 -1.9098487 0.47268671 1.8626451e-009 -1.8353341 0.47268671 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  1.85097075 0.82451272 -0.083552457 2 0.82451272 -0.083552457
		 1.85097075 1.020427227 -0.083552457 2 1.020427227 -0.083552457 2 0.82451272 -0.083552457
		 2.14902925 0.82451272 -0.083552457 2 1.020427227 -0.083552457 2.14902925 1.020427227 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev12" -p "Level_Buttons";
	setAttr ".rp" -type "double3" 0.31369533280653172 1.5261531068274863 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" 0.31369533280653172 1.5261531068274863 -0.083552457392215729 ;
createNode mesh -n "LevShape12" -p "Lev12";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.87304688 0.94921875
		 0.90429688 0.94921875 0.90429688 0.99023438 0.87304688 0.99023438 0.90527344 0.94921875
		 0.93652344 0.94921875 0.93652344 0.99023438 0.90527344 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.5025995 0.47268671 1.8626451e-009 
		-1.4280849 0.47268671 1.8626451e-009 -1.5025995 0.57064396 1.8626451e-009 -1.4280849 
		0.57064396 1.8626451e-009 -1.4280849 0.47268671 1.8626451e-009 -1.3535702 0.47268671 
		1.8626451e-009 -1.4280849 0.57064396 1.8626451e-009 -1.3535702 0.57064396 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  1.85097075 0.72655547 -0.083552457 2 0.72655547 -0.083552457
		 1.85097075 0.92246997 -0.083552457 2 0.92246997 -0.083552457 2 0.72655547 -0.083552457
		 2.14902925 0.72655547 -0.083552457 2 0.92246997 -0.083552457 2.14902925 0.92246997 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev13" -p "Level_Buttons";
	setAttr ".rp" -type "double3" -0.87337574573962984 1.199242183246112 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" -0.87337574573962984 1.199242183246112 -0.083552457392215729 ;
createNode mesh -n "LevShape13" -p "Lev13";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.87304688 0.94921875
		 0.90429688 0.94921875 0.90429688 0.99023438 0.87304688 0.99023438 0.9375 0.94921875
		 0.96875 0.94921875 0.96875 0.99023438 0.9375 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -2.9478905 0.24373305 1.8626451e-009 
		-2.8733759 0.24373305 1.8626451e-009 -2.9478905 0.3416903 1.8626451e-009 -2.8733759 
		0.3416903 1.8626451e-009 -2.8733759 0.24373305 1.8626451e-009 -2.7988613 0.24373305 
		1.8626451e-009 -2.8733759 0.3416903 1.8626451e-009 -2.7988613 0.3416903 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  1.85097075 0.62859821 -0.083552457 2 0.62859821 -0.083552457
		 1.85097075 0.82451272 -0.083552457 2 0.82451272 -0.083552457 2 0.62859821 -0.083552457
		 2.14902925 0.62859821 -0.083552457 2 0.82451272 -0.083552457 2.14902925 0.82451272 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev14" -p "Level_Buttons";
	setAttr ".rp" -type "double3" -0.64983186336963428 1.199242183246112 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" -0.64983186336963428 1.199242183246112 -0.083552457392215729 ;
createNode mesh -n "LevShape14" -p "Lev14";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.87304688 0.94921875
		 0.90429688 0.94921875 0.90429688 0.99023438 0.87304688 0.99023438 0.96777344 0.94921875
		 0.99902344 0.94921875 0.99902344 0.99023438 0.96777344 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -2.4661269 0.34169036 1.8626451e-009 
		-2.3916121 0.34169036 1.8626451e-009 -2.4661269 0.43964761 1.8626451e-009 -2.3916121 
		0.43964761 1.8626451e-009 -2.3916121 0.34169036 1.8626451e-009 -2.3170977 0.34169036 
		1.8626451e-009 -2.3916121 0.43964761 1.8626451e-009 -2.3170977 0.43964761 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  1.85097075 0.5306409 -0.083552457 2 0.5306409 -0.083552457
		 1.85097075 0.72655541 -0.083552457 2 0.72655541 -0.083552457 2 0.5306409 -0.083552457
		 2.14902925 0.5306409 -0.083552457 2 0.72655541 -0.083552457 2.14902925 0.72655541 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev15" -p "Level_Buttons";
	setAttr ".rp" -type "double3" -0.16806826528155128 1.1992421832461133 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" -0.16806826528155128 1.1992421832461133 -0.083552457392215729 ;
createNode mesh -n "LevShape15" -p "Lev15";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.87304688 0.94921875
		 0.90429688 0.94921875 0.90429688 0.99023438 0.87304688 0.99023438 0.84277344 0.88671875
		 0.87402344 0.88671875 0.87402344 0.92773438 0.84277344 0.92773438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.9843633 0.43964761 1.8626451e-009 
		-1.9098487 0.43964761 1.8626451e-009 -1.9843633 0.53760487 1.8626451e-009 -1.9098487 
		0.53760487 1.8626451e-009 -1.9098487 0.43964761 1.8626451e-009 -1.8353341 0.43964761 
		1.8626451e-009 -1.9098487 0.53760487 1.8626451e-009 -1.8353341 0.53760487 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  1.85097075 0.43268362 -0.083552457 2 0.43268362 -0.083552457
		 1.85097075 0.62859815 -0.083552457 2 0.62859815 -0.083552457 2 0.43268362 -0.083552457
		 2.14902925 0.43268362 -0.083552457 2 0.62859815 -0.083552457 2.14902925 0.62859815 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev16" -p "Level_Buttons";
	setAttr ".rp" -type "double3" 0.31369533280653172 1.199242183246112 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" 0.31369533280653172 1.199242183246112 -0.083552457392215729 ;
createNode mesh -n "LevShape16" -p "Lev16";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.87304688 0.94921875
		 0.90429688 0.94921875 0.90429688 0.99023438 0.87304688 0.99023438 0.87402344 0.88671875
		 0.90527344 0.88671875 0.90527344 0.92773438 0.87402344 0.92773438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.5025995 0.53760493 1.8626451e-009 
		-1.4280849 0.53760493 1.8626451e-009 -1.5025995 0.63556218 1.8626451e-009 -1.4280849 
		0.63556218 1.8626451e-009 -1.4280849 0.53760493 1.8626451e-009 -1.3535702 0.53760493 
		1.8626451e-009 -1.4280849 0.63556218 1.8626451e-009 -1.3535702 0.63556218 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  1.85097075 0.33472633 -0.083552457 2 0.33472633 -0.083552457
		 1.85097075 0.5306409 -0.083552457 2 0.5306409 -0.083552457 2 0.33472633 -0.083552457
		 2.14902925 0.33472633 -0.083552457 2 0.5306409 -0.083552457 2.14902925 0.5306409 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev17" -p "Level_Buttons";
	setAttr ".rp" -type "double3" -0.87337574573962984 0.8723312596647399 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" -0.87337574573962984 0.8723312596647399 -0.083552457392215729 ;
createNode mesh -n "LevShape17" -p "Lev17";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.87304688 0.94921875
		 0.90429688 0.94921875 0.90429688 0.99023438 0.87304688 0.99023438 0.90527344 0.88671875
		 0.93652344 0.88671875 0.93652344 0.92773438 0.90527344 0.92773438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -2.9478905 0.30865127 1.8626451e-009 
		-2.8733759 0.30865127 1.8626451e-009 -2.9478905 0.40660855 1.8626451e-009 -2.8733759 
		0.40660855 1.8626451e-009 -2.8733759 0.30865127 1.8626451e-009 -2.7988613 0.30865127 
		1.8626451e-009 -2.8733759 0.40660855 1.8626451e-009 -2.7988613 0.40660855 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  1.85097075 0.23676905 -0.083552457 2 0.23676905 -0.083552457
		 1.85097075 0.43268359 -0.083552457 2 0.43268359 -0.083552457 2 0.23676905 -0.083552457
		 2.14902925 0.23676905 -0.083552457 2 0.43268359 -0.083552457 2.14902925 0.43268359 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev18" -p "Level_Buttons";
	setAttr ".rp" -type "double3" -0.64983186336963517 0.87233125966473946 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" -0.64983186336963517 0.87233125966473946 -0.083552457392215729 ;
createNode mesh -n "LevShape18" -p "Lev18";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.87304688 0.94921875
		 0.90429688 0.94921875 0.90429688 0.99023438 0.87304688 0.99023438 0.93652344 0.88671875
		 0.96777344 0.88671875 0.96777344 0.92773438 0.93652344 0.92773438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -2.4661269 0.40660852 1.8626451e-009 
		-2.3916121 0.40660852 1.8626451e-009 -2.4661269 0.50456583 1.8626451e-009 -2.3916121 
		0.50456583 1.8626451e-009 -2.3916121 0.40660852 1.8626451e-009 -2.3170977 0.40660852 
		1.8626451e-009 -2.3916121 0.50456583 1.8626451e-009 -2.3170977 0.50456583 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  1.85097075 0.1388118 -0.083552457 2 0.1388118 -0.083552457
		 1.85097075 0.33472633 -0.083552457 2 0.33472633 -0.083552457 2 0.1388118 -0.083552457
		 2.14902925 0.1388118 -0.083552457 2 0.33472633 -0.083552457 2.14902925 0.33472633 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev19" -p "Level_Buttons";
	setAttr ".rp" -type "double3" -0.16806826528155128 0.87233125966473946 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" -0.16806826528155128 0.87233125966473946 -0.083552457392215729 ;
createNode mesh -n "LevShape19" -p "Lev19";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.96777344 0.88671875
		 0.99902344 0.88671875 0.99902344 0.92773438 0.96777344 0.92773438 0.87304688 0.94921875
		 0.90429688 0.94921875 0.90429688 0.99023438 0.87304688 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.9098487 0.50456583 1.8626451e-009 
		-1.8353341 0.50456583 1.8626451e-009 -1.9098487 0.60252309 1.8626451e-009 -1.8353341 
		0.60252309 1.8626451e-009 -1.9843633 0.50456583 1.8626451e-009 -1.9098487 0.50456583 
		1.8626451e-009 -1.9843633 0.60252309 1.8626451e-009 -1.9098487 0.60252309 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  2 0.040854521 -0.083552457 2.14902925 0.040854521 -0.083552457
		 2 0.23676905 -0.083552457 2.14902925 0.23676905 -0.083552457 1.85097075 0.040854521 -0.083552457
		 2 0.040854521 -0.083552457 1.85097075 0.23676905 -0.083552457 2 0.23676905 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Lev20" -p "Level_Buttons";
	setAttr ".rp" -type "double3" 0.31369533280653172 0.87233125966473946 -0.083552457392215729 ;
	setAttr ".sp" -type "double3" 0.31369533280653172 0.87233125966473946 -0.083552457392215729 ;
createNode mesh -n "LevShape20" -p "Lev20";
	setAttr -k off ".v";
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:1]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 8 ".uvst[0].uvsp[0:7]" -type "float2" 0.90527344 0.94921875
		 0.93652344 0.94921875 0.93652344 0.99023438 0.90527344 0.99023438 0.84179688 0.94921875
		 0.87304688 0.94921875 0.87304688 0.99023438 0.84179688 0.99023438;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".pt[0:7]" -type "float3"  -1.5025995 0.60252309 1.8626451e-009 
		-1.4280849 0.60252309 1.8626451e-009 -1.5025995 0.70048034 1.8626451e-009 -1.4280849 
		0.70048034 1.8626451e-009 -1.4280849 0.60252309 1.8626451e-009 -1.3535702 0.60252309 
		1.8626451e-009 -1.4280849 0.70048034 1.8626451e-009 -1.3535702 0.70048034 1.8626451e-009;
	setAttr -s 8 ".vt[0:7]"  1.85097075 -0.057102755 -0.083552457 2 -0.057102755 -0.083552457
		 1.85097075 0.13881178 -0.083552457 2 0.13881178 -0.083552457 2 -0.057102755 -0.083552457
		 2.14902925 -0.057102755 -0.083552457 2 0.13881178 -0.083552457 2.14902925 0.13881178 -0.083552457;
	setAttr -s 8 ".ed[0:7]"  0 1 0 0 2 0 1 3 0 2 3 0 4 5 0 4 6 0 5 7 0
		 6 7 0;
	setAttr -s 2 ".fc[0:1]" -type "polyFaces" 
		f 4 0 2 -4 -2
		mu 0 4 0 1 2 3
		f 4 4 6 -8 -6
		mu 0 4 4 5 6 7;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 17 ".lnk";
	setAttr -s 17 ".slnk";
createNode displayLayerManager -n "layerManager";
	setAttr ".cdl" 3;
	setAttr -s 7 ".dli";
	setAttr ".dli[2:7]" 1 2 3 4 0 5;
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n"
		+ "                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n"
		+ "            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n"
		+ "            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n"
		+ "                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n"
		+ "                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n"
		+ "            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n"
		+ "\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n"
		+ "                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n"
		+ "            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 1\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"ogsRenderer\" \n                -colorResolution 256 256 \n"
		+ "                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -shadows 0\n                $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n"
		+ "            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"ogsRenderer\" \n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n"
		+ "            -shadows 0\n            $editorName;\nmodelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n"
		+ "                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n"
		+ "                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n"
		+ "            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n"
		+ "                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n"
		+ "                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n"
		+ "            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n"
		+ "                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n"
		+ "                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n"
		+ "                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n"
		+ "                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n"
		+ "\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n"
		+ "                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n"
		+ "                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n"
		+ "            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n"
		+ "                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n"
		+ "                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -shadows 0\n"
		+ "                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                $editorName;\nstereoCameraView -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n"
		+ "                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n"
		+ "                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -shadows 0\n                -displayMode \"centerEye\" \n"
		+ "                -viewColor 0 0 0 1 \n                $editorName;\nstereoCameraView -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"multiListerPanel\" (localizedPanelLabel(\"Multilister\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"multiListerPanel\" -l (localizedPanelLabel(\"Multilister\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Multilister\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"devicePanel\" (localizedPanelLabel(\"Devices\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tdevicePanel -unParent -l (localizedPanelLabel(\"Devices\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tdevicePanel -edit -l (localizedPanelLabel(\"Devices\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"webBrowserPanel\" (localizedPanelLabel(\"Web Browser\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"webBrowserPanel\" -l (localizedPanelLabel(\"Web Browser\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Web Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n"
		+ "                -mergeConnections 1\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 1\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 50 100 -ps 2 50 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Hypershade\")) \n\t\t\t\t\t\"scriptedPanel\"\n\t\t\t\t\t\"$panelName = `scriptedPanel -unParent  -type \\\"hyperShadePanel\\\" -l (localizedPanelLabel(\\\"Hypershade\\\")) -mbv $menusOkayInPanels `\"\n\t\t\t\t\t\"scriptedPanel -edit -l (localizedPanelLabel(\\\"Hypershade\\\")) -mbv $menusOkayInPanels  $panelName\"\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"ogsRenderer\\\" \\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"ogsRenderer\\\" \\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 4.9999999999999991 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 32 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode transformGeometry -n "transformGeometry2";
createNode displayLayer -n "iPhone_Guides";
	setAttr ".do" 1;
createNode file -n "file1";
	setAttr ".ftn" -type "string" "C:/Users/Rinzler/Documents/maya/projects/CrackOLanternSAGA//Assets/Materials/Textures/CrackoSaga01.png";
createNode place2dTexture -n "place2dTexture1";
createNode blinn -n "blinn1";
createNode shadingEngine -n "blinn1SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
createNode file -n "file3";
	setAttr ".ftn" -type "string" "C:/Users/Rinzler/Documents/maya/projects/CrackOLanternSAGA//Assets/GUI Assets/Backgrounds/Level1Back.png";
createNode place2dTexture -n "place2dTexture3";
createNode lambert -n "CrackOSaga01";
createNode shadingEngine -n "lambert4SG";
	setAttr ".ihi" 0;
	setAttr -s 12 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo5";
createNode blinn -n "blinn3";
createNode shadingEngine -n "blinn3SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo6";
createNode file -n "file6";
	setAttr ".ftn" -type "string" "C:/Users/Rinzler/Documents/maya/projects/CrackOLanternSAGA//Assets/GUI Assets/Backgrounds/Level5Back.png";
createNode place2dTexture -n "place2dTexture6";
createNode blinn -n "blinn4";
createNode shadingEngine -n "blinn4SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo7";
createNode file -n "file7";
	setAttr ".ftn" -type "string" "C:/Users/Rinzler/Documents/maya/projects/CrackOLanternSAGA//Assets/GUI Assets/Backgrounds/Level10Back.png";
createNode place2dTexture -n "place2dTexture7";
createNode blinn -n "blinn5";
createNode shadingEngine -n "blinn5SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo8";
createNode file -n "file8";
	setAttr ".ftn" -type "string" "C:/Users/Rinzler/Documents/maya/projects/CrackOLanternSAGA//Assets/GUI Assets/Backgrounds/Level15Back.png";
createNode place2dTexture -n "place2dTexture8";
createNode animCurveTU -n "BackGround_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "BackGround3_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "BackGround1_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "BackGround2_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Pause_Norm_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Pause_Over_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Sound_On_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Sound_Over_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Sound_Off_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode displayLayer -n "Level_Base";
	setAttr ".do" 2;
createNode lambert -n "Main_Menu_Mat";
createNode shadingEngine -n "lambert5SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo9";
createNode file -n "file9";
	setAttr ".ftn" -type "string" "C:/Users/Rinzler/Documents/maya/projects/CrackOLanternSAGA//Assets/GUI Assets/TexturesUI/StartBack.png";
createNode place2dTexture -n "place2dTexture9";
createNode lambert -n "CrackOSaga02";
createNode shadingEngine -n "lambert6SG";
	setAttr ".ihi" 0;
	setAttr -s 44 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 20 ".gn";
createNode materialInfo -n "materialInfo10";
createNode file -n "file10";
	setAttr ".ftn" -type "string" "C:/Users/Rinzler/Documents/maya/projects/CrackOLanternSAGA//Assets/Materials/Textures/CrackoSaga02.png";
createNode place2dTexture -n "place2dTexture10";
createNode animCurveTU -n "Impass_Norm_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Impass_Over_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Easy_Over_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Hard_Over_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Easy_Norm_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Hard_Norm_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Resume_Norm_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode displayLayer -n "Main_Menu";
	setAttr ".do" 3;
createNode lambert -n "Pause_Menu_Mat";
createNode shadingEngine -n "lambert7SG";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo11";
createNode file -n "file11";
	setAttr ".ftn" -type "string" "C:/Users/Rinzler/Documents/maya/projects/CrackOLanternSAGA//Assets/GUI Assets/TexturesUI/resumeHud.png";
createNode place2dTexture -n "place2dTexture11";
createNode groupId -n "groupId101";
	setAttr ".ihi" 0;
createNode groupId -n "groupId102";
	setAttr ".ihi" 0;
createNode groupId -n "groupId103";
	setAttr ".ihi" 0;
createNode groupId -n "groupId104";
	setAttr ".ihi" 0;
createNode groupId -n "groupId105";
	setAttr ".ihi" 0;
createNode groupId -n "groupId106";
	setAttr ".ihi" 0;
createNode groupId -n "groupId107";
	setAttr ".ihi" 0;
createNode groupId -n "groupId108";
	setAttr ".ihi" 0;
createNode groupId -n "groupId109";
	setAttr ".ihi" 0;
createNode groupId -n "groupId110";
	setAttr ".ihi" 0;
createNode groupId -n "groupId111";
	setAttr ".ihi" 0;
createNode groupId -n "groupId112";
	setAttr ".ihi" 0;
createNode groupId -n "groupId113";
	setAttr ".ihi" 0;
createNode groupId -n "groupId114";
	setAttr ".ihi" 0;
createNode groupId -n "groupId115";
	setAttr ".ihi" 0;
createNode groupId -n "groupId116";
	setAttr ".ihi" 0;
createNode groupId -n "groupId117";
	setAttr ".ihi" 0;
createNode groupId -n "groupId118";
	setAttr ".ihi" 0;
createNode groupId -n "groupId119";
	setAttr ".ihi" 0;
createNode groupId -n "groupId120";
	setAttr ".ihi" 0;
createNode displayLayer -n "Pause_Menu";
	setAttr ".do" 4;
createNode animCurveTU -n "Lev20_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev01_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev02_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev03_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev04_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev05_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev06_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev07_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev08_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev09_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev10_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev11_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev12_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev13_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev14_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev15_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev16_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev17_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev18_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Lev19_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "LevelsMenu_translateX";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  3 0 32 0;
createNode animCurveTL -n "LevelsMenu_translateY";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  3 -4.25 32 0;
createNode animCurveTL -n "LevelsMenu_translateZ";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  3 0.5 32 0.5;
createNode displayLayer -n "Levels_Menu";
	setAttr ".do" 5;
createNode animCurveTU -n "Num1_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Resume_Over_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Instruct_Over_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Instruct_Norm_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Sound_Off_visibility1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Sound_Over_visibility1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Sound_On_visibility1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Num0_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Num9_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Num8_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Num7_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Num6_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Num5_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Num4_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Num3_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Num2_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "PauseMenu_translateX";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  3 0 32 0;
createNode animCurveTL -n "PauseMenu_translateY";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  3 4.25 32 0;
createNode animCurveTL -n "PauseMenu_translateZ";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  3 0.3 32 0.3;
createNode animCurveTL -n "MainMenu_translateX";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  3 -3.5 32 0;
createNode animCurveTL -n "MainMenu_translateY";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  3 0 32 0;
createNode animCurveTL -n "MainMenu_translateZ";
	setAttr ".tan" 10;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  3 0 32 0;
createNode animCurveTU -n "Main_Over_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Resume_Over_visibility1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Resume_Norm_visibility1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTU -n "Main_Norm_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 2 0;
	setAttr -s 2 ".kot[0:1]"  5 5;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 10 ".st";
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 10 ".s";
select -ne :defaultTextureList1;
	setAttr -s 8 ".tx";
select -ne :lambert1;
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -s 8 ".u";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :initialMaterialInfo;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 10 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Motion Trails"  ;
	setAttr ".otfva" -type "Int32Array" 10 1 1 1 1 1 1
		 1 1 1 1 ;
	setAttr ".cbr" 512;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :hyperGraphLayout;
	setAttr ".hyp[0].isc" yes;
connectAttr "iPhone_Guides.di" "OuterGuide.do";
connectAttr "iPhone_Guides.di" "ActionGuide.do";
connectAttr "Level_Base.di" "LevelBase.do";
connectAttr "BackGround_visibility.o" "BackGround1.v";
connectAttr "BackGround1_visibility.o" "BackGround5.v";
connectAttr "BackGround2_visibility.o" "BackGround10.v";
connectAttr "BackGround3_visibility.o" "BackGround15.v";
connectAttr "Sound_Off_visibility.o" "|iPhone|LevelBase|Sound|Sound_Off.v";
connectAttr "Sound_On_visibility.o" "|iPhone|LevelBase|Sound|Sound_On.v";
connectAttr "Sound_Over_visibility.o" "|iPhone|LevelBase|Sound|Sound_Over.v";
connectAttr "Pause_Over_visibility.o" "Pause_Over.v";
connectAttr "Pause_Norm_visibility.o" "Pause_Norm.v";
connectAttr "Main_Menu.di" "MainMenu.do";
connectAttr "MainMenu_translateX.o" "MainMenu.tx";
connectAttr "MainMenu_translateY.o" "MainMenu.ty";
connectAttr "MainMenu_translateZ.o" "MainMenu.tz";
connectAttr "Impass_Over_visibility.o" "Impass_Over.v";
connectAttr "Easy_Over_visibility.o" "Easy_Over.v";
connectAttr "Hard_Over_visibility.o" "Hard_Over.v";
connectAttr "Easy_Norm_visibility.o" "Easy_Norm.v";
connectAttr "Hard_Norm_visibility.o" "Hard_Norm.v";
connectAttr "Impass_Norm_visibility.o" "Impass_Norm.v";
connectAttr "Resume_Norm_visibility.o" "|iPhone|MainMenu|Buttons|Resume_Norm.v";
connectAttr "Resume_Over_visibility.o" "|iPhone|MainMenu|Buttons|Resume_Over.v";
connectAttr "Instruct_Over_visibility.o" "Instruct_Over.v";
connectAttr "Instruct_Norm_visibility.o" "Instruct_Norm.v";
connectAttr "defaultLayer.di" "Sound1.do";
connectAttr "Sound_Off_visibility1.o" "|iPhone|MainMenu|Sound1|Sound_Off.v";
connectAttr "Sound_On_visibility1.o" "|iPhone|MainMenu|Sound1|Sound_On.v";
connectAttr "Sound_Over_visibility1.o" "|iPhone|MainMenu|Sound1|Sound_Over.v";
connectAttr "defaultLayer.di" "Num0.do";
connectAttr "Num0_visibility.o" "Num0.v";
connectAttr "defaultLayer.di" "Num1.do";
connectAttr "Num1_visibility.o" "Num1.v";
connectAttr "defaultLayer.di" "Num2.do";
connectAttr "Num2_visibility.o" "Num2.v";
connectAttr "defaultLayer.di" "Num3.do";
connectAttr "Num3_visibility.o" "Num3.v";
connectAttr "defaultLayer.di" "Num4.do";
connectAttr "Num4_visibility.o" "Num4.v";
connectAttr "defaultLayer.di" "Num5.do";
connectAttr "Num5_visibility.o" "Num5.v";
connectAttr "defaultLayer.di" "Num6.do";
connectAttr "Num6_visibility.o" "Num6.v";
connectAttr "defaultLayer.di" "Num7.do";
connectAttr "Num7_visibility.o" "Num7.v";
connectAttr "defaultLayer.di" "Num8.do";
connectAttr "Num8_visibility.o" "Num8.v";
connectAttr "defaultLayer.di" "Num9.do";
connectAttr "Num9_visibility.o" "Num9.v";
connectAttr "Pause_Menu.di" "PauseMenu.do";
connectAttr "PauseMenu_translateX.o" "PauseMenu.tx";
connectAttr "PauseMenu_translateY.o" "PauseMenu.ty";
connectAttr "PauseMenu_translateZ.o" "PauseMenu.tz";
connectAttr "Resume_Over_visibility1.o" "|iPhone|PauseMenu|Buttons|Resume_Over.v"
		;
connectAttr "Resume_Norm_visibility1.o" "|iPhone|PauseMenu|Buttons|Resume_Norm.v"
		;
connectAttr "Main_Over_visibility.o" "Main_Over.v";
connectAttr "Main_Norm_visibility.o" "Main_Norm.v";
connectAttr "LevelsMenu_translateX.o" "LevelsMenu.tx";
connectAttr "LevelsMenu_translateY.o" "LevelsMenu.ty";
connectAttr "LevelsMenu_translateZ.o" "LevelsMenu.tz";
connectAttr "Levels_Menu.di" "LevelsMenu.do";
connectAttr "Lev01_visibility.o" "Lev01.v";
connectAttr "groupId101.id" "Lev0Shape1.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "Lev0Shape1.iog.og[0].gco";
connectAttr "Lev02_visibility.o" "Lev02.v";
connectAttr "groupId102.id" "Lev0Shape2.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "Lev0Shape2.iog.og[0].gco";
connectAttr "Lev03_visibility.o" "Lev03.v";
connectAttr "groupId103.id" "Lev0Shape3.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "Lev0Shape3.iog.og[0].gco";
connectAttr "Lev04_visibility.o" "Lev04.v";
connectAttr "groupId104.id" "Lev0Shape4.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "Lev0Shape4.iog.og[0].gco";
connectAttr "Lev05_visibility.o" "Lev05.v";
connectAttr "groupId105.id" "Lev0Shape5.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "Lev0Shape5.iog.og[0].gco";
connectAttr "Lev06_visibility.o" "Lev06.v";
connectAttr "groupId106.id" "Lev0Shape6.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "Lev0Shape6.iog.og[0].gco";
connectAttr "Lev07_visibility.o" "Lev07.v";
connectAttr "groupId107.id" "Lev0Shape7.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "Lev0Shape7.iog.og[0].gco";
connectAttr "Lev08_visibility.o" "Lev08.v";
connectAttr "groupId108.id" "Lev0Shape8.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "Lev0Shape8.iog.og[0].gco";
connectAttr "Lev09_visibility.o" "Lev09.v";
connectAttr "groupId109.id" "Lev0Shape9.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "Lev0Shape9.iog.og[0].gco";
connectAttr "Lev10_visibility.o" "Lev10.v";
connectAttr "groupId110.id" "LevShape10.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "LevShape10.iog.og[0].gco";
connectAttr "Lev11_visibility.o" "Lev11.v";
connectAttr "groupId111.id" "LevShape11.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "LevShape11.iog.og[0].gco";
connectAttr "Lev12_visibility.o" "Lev12.v";
connectAttr "groupId112.id" "LevShape12.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "LevShape12.iog.og[0].gco";
connectAttr "Lev13_visibility.o" "Lev13.v";
connectAttr "groupId113.id" "LevShape13.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "LevShape13.iog.og[0].gco";
connectAttr "Lev14_visibility.o" "Lev14.v";
connectAttr "groupId114.id" "LevShape14.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "LevShape14.iog.og[0].gco";
connectAttr "Lev15_visibility.o" "Lev15.v";
connectAttr "groupId115.id" "LevShape15.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "LevShape15.iog.og[0].gco";
connectAttr "Lev16_visibility.o" "Lev16.v";
connectAttr "groupId116.id" "LevShape16.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "LevShape16.iog.og[0].gco";
connectAttr "Lev17_visibility.o" "Lev17.v";
connectAttr "groupId117.id" "LevShape17.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "LevShape17.iog.og[0].gco";
connectAttr "Lev18_visibility.o" "Lev18.v";
connectAttr "groupId118.id" "LevShape18.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "LevShape18.iog.og[0].gco";
connectAttr "Lev19_visibility.o" "Lev19.v";
connectAttr "groupId119.id" "LevShape19.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "LevShape19.iog.og[0].gco";
connectAttr "Lev20_visibility.o" "Lev20.v";
connectAttr "groupId120.id" "LevShape20.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "LevShape20.iog.og[0].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn4SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn5SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert5SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert6SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert7SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn4SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn5SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert5SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert6SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert7SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "layerManager.dli[2]" "iPhone_Guides.id";
connectAttr "place2dTexture1.c" "file1.c";
connectAttr "place2dTexture1.tf" "file1.tf";
connectAttr "place2dTexture1.rf" "file1.rf";
connectAttr "place2dTexture1.mu" "file1.mu";
connectAttr "place2dTexture1.mv" "file1.mv";
connectAttr "place2dTexture1.s" "file1.s";
connectAttr "place2dTexture1.wu" "file1.wu";
connectAttr "place2dTexture1.wv" "file1.wv";
connectAttr "place2dTexture1.re" "file1.re";
connectAttr "place2dTexture1.of" "file1.of";
connectAttr "place2dTexture1.r" "file1.ro";
connectAttr "place2dTexture1.n" "file1.n";
connectAttr "place2dTexture1.vt1" "file1.vt1";
connectAttr "place2dTexture1.vt2" "file1.vt2";
connectAttr "place2dTexture1.vt3" "file1.vt3";
connectAttr "place2dTexture1.vc1" "file1.vc1";
connectAttr "place2dTexture1.o" "file1.uv";
connectAttr "place2dTexture1.ofs" "file1.fs";
connectAttr "file3.oc" "blinn1.c";
connectAttr "blinn1.oc" "blinn1SG.ss";
connectAttr "BackGround1Shape.iog" "blinn1SG.dsm" -na;
connectAttr "blinn1SG.msg" "materialInfo3.sg";
connectAttr "blinn1.msg" "materialInfo3.m";
connectAttr "file3.msg" "materialInfo3.t" -na;
connectAttr "place2dTexture3.c" "file3.c";
connectAttr "place2dTexture3.tf" "file3.tf";
connectAttr "place2dTexture3.rf" "file3.rf";
connectAttr "place2dTexture3.mu" "file3.mu";
connectAttr "place2dTexture3.mv" "file3.mv";
connectAttr "place2dTexture3.s" "file3.s";
connectAttr "place2dTexture3.wu" "file3.wu";
connectAttr "place2dTexture3.wv" "file3.wv";
connectAttr "place2dTexture3.re" "file3.re";
connectAttr "place2dTexture3.of" "file3.of";
connectAttr "place2dTexture3.r" "file3.ro";
connectAttr "place2dTexture3.n" "file3.n";
connectAttr "place2dTexture3.vt1" "file3.vt1";
connectAttr "place2dTexture3.vt2" "file3.vt2";
connectAttr "place2dTexture3.vt3" "file3.vt3";
connectAttr "place2dTexture3.vc1" "file3.vc1";
connectAttr "place2dTexture3.o" "file3.uv";
connectAttr "place2dTexture3.ofs" "file3.fs";
connectAttr "file1.oc" "CrackOSaga01.c";
connectAttr "file1.ot" "CrackOSaga01.it";
connectAttr "CrackOSaga01.oc" "lambert4SG.ss";
connectAttr "HudShape.iog" "lambert4SG.dsm" -na;
connectAttr "Pause_NormShape.iog" "lambert4SG.dsm" -na;
connectAttr "Pause_OverShape.iog" "lambert4SG.dsm" -na;
connectAttr "Plane_LeftBrickWallShape.iog" "lambert4SG.dsm" -na;
connectAttr "Plane_GroundShape.iog" "lambert4SG.dsm" -na;
connectAttr "|iPhone|MainMenu|Sound1|Sound_Over|Sound_OverShape.iog" "lambert4SG.dsm"
		 -na;
connectAttr "|iPhone|MainMenu|Sound1|Sound_On|Sound_OnShape.iog" "lambert4SG.dsm"
		 -na;
connectAttr "|iPhone|MainMenu|Sound1|Sound_Off|Sound_OffShape.iog" "lambert4SG.dsm"
		 -na;
connectAttr "Plane_RightBrickWallShape.iog" "lambert4SG.dsm" -na;
connectAttr "|iPhone|LevelBase|Sound|Sound_Over|Sound_OverShape.iog" "lambert4SG.dsm"
		 -na;
connectAttr "|iPhone|LevelBase|Sound|Sound_On|Sound_OnShape.iog" "lambert4SG.dsm"
		 -na;
connectAttr "|iPhone|LevelBase|Sound|Sound_Off|Sound_OffShape.iog" "lambert4SG.dsm"
		 -na;
connectAttr "lambert4SG.msg" "materialInfo5.sg";
connectAttr "CrackOSaga01.msg" "materialInfo5.m";
connectAttr "file1.msg" "materialInfo5.t" -na;
connectAttr "file6.oc" "blinn3.c";
connectAttr "blinn3.oc" "blinn3SG.ss";
connectAttr "BackGround5Shape.iog" "blinn3SG.dsm" -na;
connectAttr "blinn3SG.msg" "materialInfo6.sg";
connectAttr "blinn3.msg" "materialInfo6.m";
connectAttr "file6.msg" "materialInfo6.t" -na;
connectAttr "place2dTexture6.c" "file6.c";
connectAttr "place2dTexture6.tf" "file6.tf";
connectAttr "place2dTexture6.rf" "file6.rf";
connectAttr "place2dTexture6.mu" "file6.mu";
connectAttr "place2dTexture6.mv" "file6.mv";
connectAttr "place2dTexture6.s" "file6.s";
connectAttr "place2dTexture6.wu" "file6.wu";
connectAttr "place2dTexture6.wv" "file6.wv";
connectAttr "place2dTexture6.re" "file6.re";
connectAttr "place2dTexture6.of" "file6.of";
connectAttr "place2dTexture6.r" "file6.ro";
connectAttr "place2dTexture6.n" "file6.n";
connectAttr "place2dTexture6.vt1" "file6.vt1";
connectAttr "place2dTexture6.vt2" "file6.vt2";
connectAttr "place2dTexture6.vt3" "file6.vt3";
connectAttr "place2dTexture6.vc1" "file6.vc1";
connectAttr "place2dTexture6.o" "file6.uv";
connectAttr "place2dTexture6.ofs" "file6.fs";
connectAttr "file7.oc" "blinn4.c";
connectAttr "blinn4.oc" "blinn4SG.ss";
connectAttr "BackGround10Shape.iog" "blinn4SG.dsm" -na;
connectAttr "blinn4SG.msg" "materialInfo7.sg";
connectAttr "blinn4.msg" "materialInfo7.m";
connectAttr "file7.msg" "materialInfo7.t" -na;
connectAttr "place2dTexture7.c" "file7.c";
connectAttr "place2dTexture7.tf" "file7.tf";
connectAttr "place2dTexture7.rf" "file7.rf";
connectAttr "place2dTexture7.mu" "file7.mu";
connectAttr "place2dTexture7.mv" "file7.mv";
connectAttr "place2dTexture7.s" "file7.s";
connectAttr "place2dTexture7.wu" "file7.wu";
connectAttr "place2dTexture7.wv" "file7.wv";
connectAttr "place2dTexture7.re" "file7.re";
connectAttr "place2dTexture7.of" "file7.of";
connectAttr "place2dTexture7.r" "file7.ro";
connectAttr "place2dTexture7.n" "file7.n";
connectAttr "place2dTexture7.vt1" "file7.vt1";
connectAttr "place2dTexture7.vt2" "file7.vt2";
connectAttr "place2dTexture7.vt3" "file7.vt3";
connectAttr "place2dTexture7.vc1" "file7.vc1";
connectAttr "place2dTexture7.o" "file7.uv";
connectAttr "place2dTexture7.ofs" "file7.fs";
connectAttr "file8.oc" "blinn5.c";
connectAttr "blinn5.oc" "blinn5SG.ss";
connectAttr "BackGround15Shape.iog" "blinn5SG.dsm" -na;
connectAttr "blinn5SG.msg" "materialInfo8.sg";
connectAttr "blinn5.msg" "materialInfo8.m";
connectAttr "file8.msg" "materialInfo8.t" -na;
connectAttr "place2dTexture8.c" "file8.c";
connectAttr "place2dTexture8.tf" "file8.tf";
connectAttr "place2dTexture8.rf" "file8.rf";
connectAttr "place2dTexture8.mu" "file8.mu";
connectAttr "place2dTexture8.mv" "file8.mv";
connectAttr "place2dTexture8.s" "file8.s";
connectAttr "place2dTexture8.wu" "file8.wu";
connectAttr "place2dTexture8.wv" "file8.wv";
connectAttr "place2dTexture8.re" "file8.re";
connectAttr "place2dTexture8.of" "file8.of";
connectAttr "place2dTexture8.r" "file8.ro";
connectAttr "place2dTexture8.n" "file8.n";
connectAttr "place2dTexture8.vt1" "file8.vt1";
connectAttr "place2dTexture8.vt2" "file8.vt2";
connectAttr "place2dTexture8.vt3" "file8.vt3";
connectAttr "place2dTexture8.vc1" "file8.vc1";
connectAttr "place2dTexture8.o" "file8.uv";
connectAttr "place2dTexture8.ofs" "file8.fs";
connectAttr "layerManager.dli[3]" "Level_Base.id";
connectAttr "file9.oc" "Main_Menu_Mat.c";
connectAttr "Main_Menu_Mat.oc" "lambert5SG.ss";
connectAttr "MainShape.iog" "lambert5SG.dsm" -na;
connectAttr "lambert5SG.msg" "materialInfo9.sg";
connectAttr "Main_Menu_Mat.msg" "materialInfo9.m";
connectAttr "file9.msg" "materialInfo9.t" -na;
connectAttr "place2dTexture9.c" "file9.c";
connectAttr "place2dTexture9.tf" "file9.tf";
connectAttr "place2dTexture9.rf" "file9.rf";
connectAttr "place2dTexture9.mu" "file9.mu";
connectAttr "place2dTexture9.mv" "file9.mv";
connectAttr "place2dTexture9.s" "file9.s";
connectAttr "place2dTexture9.wu" "file9.wu";
connectAttr "place2dTexture9.wv" "file9.wv";
connectAttr "place2dTexture9.re" "file9.re";
connectAttr "place2dTexture9.of" "file9.of";
connectAttr "place2dTexture9.r" "file9.ro";
connectAttr "place2dTexture9.n" "file9.n";
connectAttr "place2dTexture9.vt1" "file9.vt1";
connectAttr "place2dTexture9.vt2" "file9.vt2";
connectAttr "place2dTexture9.vt3" "file9.vt3";
connectAttr "place2dTexture9.vc1" "file9.vc1";
connectAttr "place2dTexture9.o" "file9.uv";
connectAttr "place2dTexture9.ofs" "file9.fs";
connectAttr "file10.oc" "CrackOSaga02.c";
connectAttr "file10.ot" "CrackOSaga02.it";
connectAttr "CrackOSaga02.oc" "lambert6SG.ss";
connectAttr "Easy_NormShape.iog" "lambert6SG.dsm" -na;
connectAttr "Hard_NormShape.iog" "lambert6SG.dsm" -na;
connectAttr "Impass_NormShape.iog" "lambert6SG.dsm" -na;
connectAttr "Impass_OverShape.iog" "lambert6SG.dsm" -na;
connectAttr "Easy_OverShape.iog" "lambert6SG.dsm" -na;
connectAttr "Hard_OverShape.iog" "lambert6SG.dsm" -na;
connectAttr "|iPhone|MainMenu|Buttons|Resume_Norm|Resume_NormShape.iog" "lambert6SG.dsm"
		 -na;
connectAttr "|iPhone|MainMenu|Buttons|Resume_Over|Resume_OverShape.iog" "lambert6SG.dsm"
		 -na;
connectAttr "Instruct_OverShape.iog" "lambert6SG.dsm" -na;
connectAttr "Instruct_NormShape.iog" "lambert6SG.dsm" -na;
connectAttr "Num0Shape.iog" "lambert6SG.dsm" -na;
connectAttr "Num1Shape.iog" "lambert6SG.dsm" -na;
connectAttr "Num2Shape.iog" "lambert6SG.dsm" -na;
connectAttr "Num3Shape.iog" "lambert6SG.dsm" -na;
connectAttr "Num4Shape.iog" "lambert6SG.dsm" -na;
connectAttr "Num5Shape.iog" "lambert6SG.dsm" -na;
connectAttr "Num6Shape.iog" "lambert6SG.dsm" -na;
connectAttr "Num7Shape.iog" "lambert6SG.dsm" -na;
connectAttr "Num8Shape.iog" "lambert6SG.dsm" -na;
connectAttr "Num9Shape.iog" "lambert6SG.dsm" -na;
connectAttr "|iPhone|PauseMenu|Buttons|Resume_Over|Resume_OverShape.iog" "lambert6SG.dsm"
		 -na;
connectAttr "|iPhone|PauseMenu|Buttons|Resume_Norm|Resume_NormShape.iog" "lambert6SG.dsm"
		 -na;
connectAttr "Main_OverShape.iog" "lambert6SG.dsm" -na;
connectAttr "Main_NormShape.iog" "lambert6SG.dsm" -na;
connectAttr "Lev0Shape1.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "Lev0Shape2.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "Lev0Shape3.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "Lev0Shape4.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "Lev0Shape5.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "Lev0Shape6.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "Lev0Shape7.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "Lev0Shape8.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "Lev0Shape9.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "LevShape10.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "LevShape11.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "LevShape12.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "LevShape13.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "LevShape14.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "LevShape15.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "LevShape16.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "LevShape17.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "LevShape18.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "LevShape19.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "LevShape20.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "groupId101.msg" "lambert6SG.gn" -na;
connectAttr "groupId102.msg" "lambert6SG.gn" -na;
connectAttr "groupId103.msg" "lambert6SG.gn" -na;
connectAttr "groupId104.msg" "lambert6SG.gn" -na;
connectAttr "groupId105.msg" "lambert6SG.gn" -na;
connectAttr "groupId106.msg" "lambert6SG.gn" -na;
connectAttr "groupId107.msg" "lambert6SG.gn" -na;
connectAttr "groupId108.msg" "lambert6SG.gn" -na;
connectAttr "groupId109.msg" "lambert6SG.gn" -na;
connectAttr "groupId110.msg" "lambert6SG.gn" -na;
connectAttr "groupId111.msg" "lambert6SG.gn" -na;
connectAttr "groupId112.msg" "lambert6SG.gn" -na;
connectAttr "groupId113.msg" "lambert6SG.gn" -na;
connectAttr "groupId114.msg" "lambert6SG.gn" -na;
connectAttr "groupId115.msg" "lambert6SG.gn" -na;
connectAttr "groupId116.msg" "lambert6SG.gn" -na;
connectAttr "groupId117.msg" "lambert6SG.gn" -na;
connectAttr "groupId118.msg" "lambert6SG.gn" -na;
connectAttr "groupId119.msg" "lambert6SG.gn" -na;
connectAttr "groupId120.msg" "lambert6SG.gn" -na;
connectAttr "lambert6SG.msg" "materialInfo10.sg";
connectAttr "CrackOSaga02.msg" "materialInfo10.m";
connectAttr "file10.msg" "materialInfo10.t" -na;
connectAttr "place2dTexture10.c" "file10.c";
connectAttr "place2dTexture10.tf" "file10.tf";
connectAttr "place2dTexture10.rf" "file10.rf";
connectAttr "place2dTexture10.mu" "file10.mu";
connectAttr "place2dTexture10.mv" "file10.mv";
connectAttr "place2dTexture10.s" "file10.s";
connectAttr "place2dTexture10.wu" "file10.wu";
connectAttr "place2dTexture10.wv" "file10.wv";
connectAttr "place2dTexture10.re" "file10.re";
connectAttr "place2dTexture10.of" "file10.of";
connectAttr "place2dTexture10.r" "file10.ro";
connectAttr "place2dTexture10.n" "file10.n";
connectAttr "place2dTexture10.vt1" "file10.vt1";
connectAttr "place2dTexture10.vt2" "file10.vt2";
connectAttr "place2dTexture10.vt3" "file10.vt3";
connectAttr "place2dTexture10.vc1" "file10.vc1";
connectAttr "place2dTexture10.o" "file10.uv";
connectAttr "place2dTexture10.ofs" "file10.fs";
connectAttr "layerManager.dli[4]" "Main_Menu.id";
connectAttr "file11.oc" "Pause_Menu_Mat.c";
connectAttr "Pause_Menu_Mat.oc" "lambert7SG.ss";
connectAttr "PauseShape.iog" "lambert7SG.dsm" -na;
connectAttr "LevelsShape.iog" "lambert7SG.dsm" -na;
connectAttr "lambert7SG.msg" "materialInfo11.sg";
connectAttr "Pause_Menu_Mat.msg" "materialInfo11.m";
connectAttr "file11.msg" "materialInfo11.t" -na;
connectAttr "place2dTexture11.c" "file11.c";
connectAttr "place2dTexture11.tf" "file11.tf";
connectAttr "place2dTexture11.rf" "file11.rf";
connectAttr "place2dTexture11.mu" "file11.mu";
connectAttr "place2dTexture11.mv" "file11.mv";
connectAttr "place2dTexture11.s" "file11.s";
connectAttr "place2dTexture11.wu" "file11.wu";
connectAttr "place2dTexture11.wv" "file11.wv";
connectAttr "place2dTexture11.re" "file11.re";
connectAttr "place2dTexture11.of" "file11.of";
connectAttr "place2dTexture11.r" "file11.ro";
connectAttr "place2dTexture11.n" "file11.n";
connectAttr "place2dTexture11.vt1" "file11.vt1";
connectAttr "place2dTexture11.vt2" "file11.vt2";
connectAttr "place2dTexture11.vt3" "file11.vt3";
connectAttr "place2dTexture11.vc1" "file11.vc1";
connectAttr "place2dTexture11.o" "file11.uv";
connectAttr "place2dTexture11.ofs" "file11.fs";
connectAttr "layerManager.dli[5]" "Pause_Menu.id";
connectAttr "layerManager.dli[7]" "Levels_Menu.id";
connectAttr "blinn1SG.pa" ":renderPartition.st" -na;
connectAttr "lambert4SG.pa" ":renderPartition.st" -na;
connectAttr "blinn3SG.pa" ":renderPartition.st" -na;
connectAttr "blinn4SG.pa" ":renderPartition.st" -na;
connectAttr "blinn5SG.pa" ":renderPartition.st" -na;
connectAttr "lambert5SG.pa" ":renderPartition.st" -na;
connectAttr "lambert6SG.pa" ":renderPartition.st" -na;
connectAttr "lambert7SG.pa" ":renderPartition.st" -na;
connectAttr "blinn1.msg" ":defaultShaderList1.s" -na;
connectAttr "CrackOSaga01.msg" ":defaultShaderList1.s" -na;
connectAttr "blinn3.msg" ":defaultShaderList1.s" -na;
connectAttr "blinn4.msg" ":defaultShaderList1.s" -na;
connectAttr "blinn5.msg" ":defaultShaderList1.s" -na;
connectAttr "Main_Menu_Mat.msg" ":defaultShaderList1.s" -na;
connectAttr "CrackOSaga02.msg" ":defaultShaderList1.s" -na;
connectAttr "Pause_Menu_Mat.msg" ":defaultShaderList1.s" -na;
connectAttr "file1.msg" ":defaultTextureList1.tx" -na;
connectAttr "file3.msg" ":defaultTextureList1.tx" -na;
connectAttr "file6.msg" ":defaultTextureList1.tx" -na;
connectAttr "file7.msg" ":defaultTextureList1.tx" -na;
connectAttr "file8.msg" ":defaultTextureList1.tx" -na;
connectAttr "file9.msg" ":defaultTextureList1.tx" -na;
connectAttr "file10.msg" ":defaultTextureList1.tx" -na;
connectAttr "file11.msg" ":defaultTextureList1.tx" -na;
connectAttr "file1.oc" ":lambert1.c";
connectAttr "file1.ot" ":lambert1.it";
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture3.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture6.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture7.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture8.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture9.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture10.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "place2dTexture11.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "file1.msg" ":initialMaterialInfo.t" -na;
connectAttr "Guides_GRP.msg" ":hyperGraphLayout.hyp[0].dn";
// End of CrackOSaga.ma
