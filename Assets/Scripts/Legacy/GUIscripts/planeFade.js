//define variables and initial fade values
var startWithFade : boolean = true;
var startFadeIn : boolean = true;
var startOpaque : boolean = true;
var minAlpha : float = 0;
var maxAlpha : float = 1;
var fadeSpeed : float = 2;
var loopYield : float = 0;
var delay : float = 0;
var fadeComplete : boolean = false;

private var alpha : float = 0.0;
private var countdownScript : GUIcountdown;


function Awake()
{
	if (GameObject.Find("gameGUI"))
	{
		countdownScript = GameObject.Find("gameGUI").GetComponent(GUIcountdown);	
	}
}

//begin scene by fading in or out if desired
function Start(){
    if (startOpaque)
    {
    	alpha = 1.0;	
    }
    
    if (startWithFade)
    {
    	if (startFadeIn)
    	{
    		alpha = 1.0;
    		FadeIn(minAlpha, maxAlpha, fadeSpeed, loopYield, delay);
    	}
    	else
    	{
    		alpha = 0.0;
    		FadeOut(minAlpha, maxAlpha, fadeSpeed, loopYield, delay);	
    	}
    }
}



//draws black texture over everything
function Update(){
    GetComponent.<Renderer>().material.color.a = alpha;
}


//function to fade alpha of texture to 0 creating transparency
function FadeIn (minAlphaValue : float, maxAlphaValue : float, fadeSpeedTime : float, loopYieldTime : float, delayTime : float)
{
	fadeComplete = false;
	yield WaitForSeconds(delayTime);
	
	while (alpha > minAlphaValue)
	{
		alpha -= fadeSpeedTime * Time.deltaTime;
		alpha = Mathf.Clamp(alpha, minAlphaValue, maxAlphaValue);
		yield WaitForSeconds(loopYieldTime);
	}
	fadeComplete = true;
	
	/*
	if(countdownScript)
	{
		countdownScript.enabled = true;	
	}
	*/
}


//function to fade alpha of texture to 1 creating opacity
function FadeOut (minAlphaValue : float, maxAlphaValue : float, fadeSpeedTime : float, loopYieldTime : float, delayTime : float)
{
	fadeComplete = false;
	yield WaitForSeconds(delayTime);
	
	while (alpha < maxAlphaValue)
	{
		alpha += fadeSpeedTime * Time.deltaTime;
		alpha = Mathf.Clamp(alpha, minAlphaValue, maxAlphaValue);
		yield WaitForSeconds(loopYieldTime);
	}
	fadeComplete = true;
}