//define variables and initial fade values
var fadeOutTexture : Texture2D;
var loadingTexture : Texture2D = fadeOutTexture;
var startWithFade : boolean = true;
var startFadeIn : boolean = true;
var startOpaque : boolean = true;
var minAlpha : float = 0;
var maxAlpha : float = 1;
var fadeSpeed : float = 2;
var loopYield : float = 0;
var fadeComplete : boolean = false;
var displayLoading : boolean = false;

private var delay : float = 0;
private var alpha : float = 0.0;
private var drawDepth : float = -100;
private var countdownScript : GUIcountdown;


//begin scene by fading in or out if desired
function Awake(){
    if (startOpaque)
    {
    	alpha = 1.0;	
    }
    
    if (startWithFade)
    {
    	if (startFadeIn)
    	{
    		alpha = 1.0;
    		FadeIn(minAlpha, maxAlpha, fadeSpeed, loopYield, delay);
    	}
    	else
    	{
    		alpha = 0.0;
    		FadeOut(minAlpha, maxAlpha, fadeSpeed, loopYield, delay);	
    	}
    }
}



function Start()
{
	if (GameObject.Find("gameGUI"))
	{
		countdownScript = GameObject.Find("gameGUI").GetComponent(GUIcountdown);	
	}
}



//draws black texture over everything
function OnGUI(){
    GUI.color.a = alpha;
    GUI.depth = drawDepth;
	GUI.DrawTexture(Rect(0, 0, Screen.width, Screen.height), fadeOutTexture);

	if (displayLoading && Application.loadedLevelName == "menu" )
	{
		GUI.color.a = alpha;
		GUI.DrawTexture(Rect(Screen.width - 180, Screen.height - 60, 180, 50), loadingTexture);
	}
}


//function to fade alpha of texture to 0 creating transparency
function FadeIn (minAlphaValue : float, maxAlphaValue : float, fadeSpeedTime : float, loopYieldTime : float, delayTime : float)
{
	fadeComplete = false;
	yield WaitForSeconds(delayTime);
	
	while (alpha > minAlphaValue)
	{
		alpha -= fadeSpeedTime * Time.deltaTime;
		alpha = Mathf.Clamp(alpha, minAlphaValue, maxAlphaValue);
		yield WaitForSeconds(loopYieldTime);
	}
	fadeComplete = true;
	
	
	if(countdownScript)
	{
		countdownScript.enabled = true;	
	}
	
}


//function to fade alpha of texture to 1 creating opacity
function FadeOut (minAlphaValue : float, maxAlphaValue : float, fadeSpeedTime : float, loopYieldTime : float, delayTime : float)
{
	fadeComplete = false;
	displayLoading = true;

	yield WaitForSeconds(delayTime);
	
	while (alpha < maxAlphaValue)
	{
		alpha += fadeSpeedTime * Time.deltaTime;
		alpha = Mathf.Clamp(alpha, minAlphaValue, maxAlphaValue);
		yield WaitForSeconds(loopYieldTime);
	}
		fadeComplete = true;
}