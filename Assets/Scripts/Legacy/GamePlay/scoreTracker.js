var enemyDamage : int = 0;  //will always stay at 0
var enemyHealth : int = 0; //set health through levelSettings script
var score : float = 0; //default time allowed before game ends. multiplier defined in levelSettings script
var maxScore : float = 0;
var scoreRatio : float = 1;
var numberSmashed : int = 0;
var startCounting : boolean = false;

private var levelScript : startEndLevel;
private var smashScript : smashObjects;
private var enemyObject : GameObject;


//Ensure script loads disabled
function Start()
{
	enabled = true;  //ensure script is enabled for GUIgame
				 	 //TO DELAY DISPLAY OF SCOREBAR SIMPLY DISABLE THIS SCRIPT
	levelScript = GameObject.Find("levelScripts").GetComponent(startEndLevel);
	smashScript = GameObject.Find("levelScripts").GetComponent(smashObjects);
	enemyObject = GameObject.FindWithTag("enemy");

	if (!score)
		score = 0;
}



//countdown the timer
function Update()
{
	if(score > 0 && startCounting)
	{
		score -= Time.deltaTime * scoreRatio;
	}
	
	if (enemyDamage >= enemyHealth)
	{
		smashScript.SmashEnemy(enemyObject);
		levelScript.YouWin();
		enemyDamage = enemyHealth;
		enabled = false;
	}
	else if (score <= 0)
	{
		levelScript.GameOver();	
		score = 0;
		enabled = false;
	}

	
}