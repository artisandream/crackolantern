//define variables used by spring joint and rigid body while dragging
var mass = 1.0;
var spring = 200.0;
var damper = 0.0;
var drag = 20.0;
var angularDrag = 5.0;
var distance = 0;
var attachToCenterOfMass = true;  //not really necessary, but left from template script just in case

//references to other objects
private var objectGrabbed = false;
private var selectionRing : GameObject;
private var innerRing : GameObject;
private var springJoint : SpringJoint;
private var sfx : sfxScript;


function Awake()
{
	selectionRing = transform.Find ("selectionRing").gameObject;
	//innerRing = selectionRing.transform.Find("innerRing").gameObject;
	springJoint = GetComponent(SpringJoint);
	sfx = GameObject.Find("SFX").GetComponent(sfxScript);
}



function Update ()
{
	//Don't do anything if the game is "paused"
	if (Time.timeScale == 0)
		return;
		
	//Make sure the user touched iPhone
	if (!(Input.touchCount > 0 && ( (Input.GetTouch(0).phase == TouchPhase.Began) || (Input.GetTouch(0).phase == TouchPhase.Moved && !objectGrabbed) )))
		return;

	//get touch position and convert to Vector3 for raycasting
	var touchPos : Vector2 = Input.GetTouch(0).position;
	var touchPosInput : Vector3 = Vector3(touchPos.x, touchPos.y, 0);
	var mainCamera = FindCamera();

	// We need to actually hit an object
	var hit : RaycastHit;
	if (!Physics.Raycast(mainCamera.ScreenPointToRay(touchPosInput),  hit, 100))
		return;
	
	// We need to hit a rigidbody that is not kinematic
	if (!hit.rigidbody || hit.rigidbody.isKinematic)
		return;
	
	springJoint.transform.position = hit.point;
	
	if (attachToCenterOfMass)
	{
		var anchor = transform.TransformDirection(hit.rigidbody.centerOfMass) + hit.rigidbody.transform.position;
		anchor = springJoint.transform.InverseTransformPoint(anchor);
		springJoint.anchor = anchor;
	}
	else
	{
		springJoint.anchor = Vector3.zero;
	}

	//set springjoint attributes based on variables and connect to rigidbody
	springJoint.spring = spring;
	springJoint.damper = damper;
	springJoint.maxDistance = distance;
	springJoint.connectedBody = hit.rigidbody;
	
	sfx.PlayPumpkinSFX(3);
	
	//run coroutine
	DragObject(hit.distance);
}


//part of the script that actually moves dragger around bringing rigidbody along
function DragObject (distance : float)
{
	objectGrabbed = true;
	//save original values for after drag
	var oldDrag = springJoint.connectedBody.drag;
	var oldAngularDrag = springJoint.connectedBody.angularDrag;
	var oldMass = springJoint.connectedBody.mass;
	springJoint.connectedBody.drag = drag;
	springJoint.connectedBody.angularDrag = angularDrag;
	springJoint.connectedBody.mass = mass;
	var outerRingSpeed : float = -.8;
	
	var mainCamera = FindCamera();
	
	ShowRing();  //show the selection ring

	
	//******PLAY AUDIO*****//
	
	
	//while user is still touching screen and rigidbody still exists, move dragger to touch position
	while (Input.touchCount > 0 && springJoint.connectedBody && enabled)
	{
		var pos : Vector2 = Input.GetTouch(0).position;
		var posInput : Vector3 = Vector3(pos.x, pos.y, 0);
		
		var ray = mainCamera.ScreenPointToRay (posInput);
		springJoint.transform.position = ray.GetPoint(distance);
		selectionRing.transform.position = springJoint.connectedBody.transform.position;
		selectionRing.transform.Rotate (0, 0, outerRingSpeed);
		//innerRing.transform.Rotate (0, 0, innerRingSpeed);

		yield;
	}
	
	//reassign original values back to rigidbody
	if (springJoint.connectedBody)
	{
		springJoint.connectedBody.drag = oldDrag;
		springJoint.connectedBody.angularDrag = oldAngularDrag;
		springJoint.connectedBody.mass = oldMass;	
	}

	HideRing(); //hide the selection ring
	
	//remove connected body in case object was incinerated
	springJoint.connectedBody = null;
	objectGrabbed = false;
}



function FindCamera ()
{
	if (GetComponent.<Camera>())
		return GetComponent.<Camera>();
	else
		return Camera.main;
}



function ShowRing ()
{
	//selectionRing.renderer.enabled = true;
	//innerRing.renderer.enabled = true;
	for (var child : Transform in selectionRing.transform)
	{
		if (child.GetComponent.<Renderer>())
			child.GetComponent.<Renderer>().enabled = true;	
	}
	/*
	for (var child : Transform in innerRing.transform)
	{
		if (child.renderer)
			child.renderer.enabled = true;	
	}
	*/
}



function HideRing ()
{
	//selectionRing.renderer.enabled = false;
	//innerRing.renderer.enabled = false;
	for (var child : Transform in selectionRing.transform)
	{
		if (child.GetComponent.<Renderer>())
			child.GetComponent.<Renderer>().enabled = false;	
	}
	/*
	for (var child : Transform in innerRing.transform)
	{
		if (child.renderer)
			child.renderer.enabled = false;	
	}
	*/
}