var splashTexture : Texture2D;
var splashTextureHD : Texture2D;
private var fadeScript : cameraFade;

function Awake()
{
	Application.PreloadLevel("help");
	Application.PreloadLevel("level01");
	fadeScript = Camera.main.GetComponent(cameraFade);
}


function OnGUI () {
    if(Screen.width < 700){
        GUI.DrawTexture(Rect (0, 0, Screen.width, Screen.height), splashTexture);
    } else {
        GUI.DrawTexture(Rect (0, 0, Screen.width, Screen.height), splashTextureHD);

    }
}


function Update()
{
	if (fadeScript.fadeComplete)
	{
		//****IF LEVEL EXISTS IN PLAYERPREFS THEN LOAD RESUME SCREEN****//
		//****ELSE LOAD MENU****//
		
			StartMe();		
	}	
}
function StartMe() {
	yield WaitForSeconds(3);
	Application.LoadLevel("menu");
}