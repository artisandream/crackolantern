var smashParticles : GameObject;
var fireParticles : GameObject;
var iceParticles : GameObject;
var bombParticles : GameObject;
var enemyParticles : GameObject;


private var scoreScript : scoreTracker;
private var sfx : sfxScript;
private var ghostExplodeScript : ghostExploder;


function Start()
{
	scoreScript = GameObject.Find("levelScripts").GetComponent(scoreTracker);
	sfx = GameObject.Find("SFX").GetComponent(sfxScript);
	ghostExplodeScript = GameObject.Find("levelScripts").GetComponent(ghostExploder);
}


function Smash(object : GameObject)
{
	//ensure an object has been passed
	if (!object)
		return;

	
	
	//define variables after checks above
	var difficultySetting : String = PlayerPrefs.GetString("Difficulty");

	
	switch (difficultySetting)
	{
		case "Easy":
			addScore = 4;
			break;
		case "Hard":
			addScore = 2;
			break;
		case "Impassible":
			addScore = 1;
			break;
		default:
			addScore = 4;
			break;
	}

	
	//var smashed : int = 1;  //actual number of smashed pumpkins. now refleted by points scored by pumpkins
	var particlePrefab : GameObject = smashParticles;
	var smashSound : int = 0;
	var createGhost = true;
	
	if (object.tag == "fire")
	{
    	switch (difficultySetting)
    	{
    		case "Easy":
    			addScore = 5;
    			break;
    		case "Hard":
    			addScore = 3;
    			break;
    		case "Impassible":
    			addScore = 2;
    			break;
    		default:
    			addScore = 5;
    			break;
    	}
    	
		particlePrefab = fireParticles;
	}
	else if (object.tag == "ice")
	{
		smashSound = 5;  //need to set correct index
		particlePrefab = iceParticles;
	}	
	else if (object.tag == "bomb")
	{
		smashSound = 4;
		addScore = 0;
		smashed = 0;
		particlePrefab = bombParticles;
	}
	else if (object.tag == "iceHazard")
	{
		smashSound = 5;  //need to set correct index
		addScore = 0;
		smashed = 0;
		particlePrefab = iceParticles;
		createGhost = false;
	}
		
	if (!GameObject.Find("particleGroup"))
	{	
		particleGroup = new GameObject ("particleGroup");
	}
	
	var newParticles : GameObject = Instantiate (particlePrefab, object.transform.position + Vector3(0, 0, .25), Quaternion.identity);
	newParticles.transform.parent = GameObject.Find ("particleGroup").transform;

	if (createGhost)
		ghostExplodeScript.CreateGhost(addScore, object.transform.position);
	
	//******PLAY AUDIO******//
	sfx.PlayPumpkinSFX(smashSound);
	
	if (scoreScript.enabled)
	{
		if ((scoreScript.score + addScore) < scoreScript.maxScore)
		{
			scoreScript.score += addScore;
		}
		else
		{
			scoreScript.score = scoreScript.maxScore;
		}
		
		scoreScript.numberSmashed += addScore;	
	}
	
	Destroy(object);

}


function SmashDamage(object : GameObject, enemyObject : GameObject)
{
	//ensure an object has been passed
	if (!object)
		return;

	

	//define variables after checks above
	var addDamage : int = 1;
	var smashed : int = 1;  //actual number of smashed pumpkins. now refleted by points scored by pumpkins
	var shakeTimer : float = .35;
	var particlePrefab : GameObject = smashParticles;
	var smashSound : int = 0;
	var createGhost = false;
	
	if (object.tag == "fire")
	{
		addDamage = 2;
		particlePrefab = fireParticles;
	}
	
	if (object.tag == "bomb")
	{
		addDamage = 4;
		smashSound = 4;
		particlePrefab = bombParticles;	
	}
	
		
	if (!GameObject.Find("particleGroup"))
	{	
		particleGroup = new GameObject ("particleGroup");
	}
	
	var newParticles : GameObject = Instantiate (particlePrefab, object.transform.position + Vector3(0, 0, .25), Quaternion.identity);
	newParticles.transform.parent = GameObject.Find ("particleGroup").transform;

	if (createGhost)
		ghostExplodeScript.CreateGhost(1, object.transform.position);
	
	//******PLAY AUDIO******//
	sfx.PlayPumpkinSFX(smashSound);
	
	if (scoreScript.enabled)
	{
		scoreScript.enemyDamage += addDamage;
		scoreScript.numberSmashed++;	
	}

	Destroy(object);

	//if enemy is destroyed don't access following scripts or will error out
	if (scoreScript.enemyDamage >= scoreScript.enemyHealth)
		return;
				
	//change opacity of damagePlane
	if (enemyObject.transform.Find("damagePlane"))
	{
	enemyObject.transform.Find("damagePlane").GetComponent.<Renderer>().material.color.a = ((1.0 * scoreScript.enemyDamage) / scoreScript.enemyHealth);
	}
	//make enemy object shake showing damage
	var initialPosition : Vector3 = enemyObject.transform.position;
	while (shakeTimer > 0)
	{
		enemyObject.transform.position = Vector3(initialPosition.x + Random.Range(-.045,.045), initialPosition.y + Random.Range(0,.02), initialPosition.z);
		shakeTimer -= (Time.deltaTime);
		yield (Time.deltaTime);
		
	}
	enemyObject.transform.position = initialPosition;
}


function SmashOnHazard(object : GameObject)
{
	//ensure an object has been passed
	if (!object)
		return;

	

	//define variables after checks above
	var particlePrefab : GameObject = smashParticles;
	var smashSound : int = 0;
	
	if (object.tag == "fire")
	{
		//smashSound = 2;
		particlePrefab = fireParticles;
	}
	else if (object.tag == "ice")
	{
		//smashSound = 5;  //need to set correct index
		particlePrefab = iceParticles;
	}	
	else if (object.tag == "bomb")
	{
		//smashSound = 4;
		particlePrefab = bombParticles;
	}
	
		
	if (!GameObject.Find("particleGroup"))
	{	
		particleGroup = new GameObject ("particleGroup");
	}
	
	var newParticles : GameObject = Instantiate (particlePrefab, object.transform.position + Vector3(0, 0, .25), Quaternion.identity);
	newParticles.transform.parent = GameObject.Find ("particleGroup").transform;
	
	
	//******PLAY AUDIO******//
	sfx.PlayPumpkinSFX(smashSound);
	
	ghostExplodeScript.CreateGhost(0, object.transform.position);
	
	Destroy(object);

}


function SmashEnemy(object : GameObject)
{
	//ensure an object has been passed
	if (!object)
		return;

	

	//define variables after checks above
	var addScore : int = 1;
	var particlePrefab : GameObject = enemyParticles;
	var smashSound : int = 0;
		
	if (!GameObject.Find("particleGroup"))
	{	
		particleGroup = new GameObject ("particleGroup");
	}
	
	var newParticles : GameObject = Instantiate (particlePrefab, object.transform.position + Vector3(0, 0, .25), Quaternion.identity);
	newParticles.transform.parent = GameObject.Find ("particleGroup").transform;

	//******PLAY AUDIO******//
	sfx.PlayPumpkinSFX(smashSound);
	
	if (scoreScript.enabled)
	{
		scoreScript.numberSmashed += addScore;	
	}
	
	Destroy(object);
}



function SmashAll()
{
	//find all interactive objects in the scene
	var popDelay : float = .05;
	var allPumpkins : GameObject[];
	allPumpkins = GameObject.FindGameObjectsWithTag ("pumpkin");
	var allCrackeds : GameObject[];
	allCrackeds = GameObject.FindGameObjectsWithTag ("cracked");
	var allFires : GameObject[];
	allFires = GameObject.FindGameObjectsWithTag ("fire");
	var allIce : GameObject[];
	allIce = GameObject.FindGameObjectsWithTag ("ice");
	var allBombs : GameObject[];
	allBombs = GameObject.FindGameObjectsWithTag ("bomb");
	var allIceHazard : GameObject[];
	allIceHazard = GameObject.FindGameObjectsWithTag ("iceHazard");
	
	
	//cycle through each array and smash each element with short delay
	if (allBombs.length > 0){
		for (object in allBombs){
			Smash(object);	
		}
	}
	
	if (allPumpkins.length > 0){
		for (object in allPumpkins){
			Smash(object);	
			yield WaitForSeconds(popDelay);
		}
	}
	
	if (allCrackeds.length > 0){
		for (object in allCrackeds){
			Smash(object);
			yield WaitForSeconds(popDelay);	
		}
	}
	
	if (allFires.length > 0){
		for (object in allFires){
			Smash(object);
			yield WaitForSeconds(popDelay);	
		}
	}
	
	if (allIce.length > 0){
		for (object in allIce){
			Smash(object);
			yield WaitForSeconds(popDelay);	
		}
	}
	
	if (allIceHazard.length > 0){
		for (object in allIceHazard){
			Smash(object);
			yield WaitForSeconds(popDelay);	
		}
	}	
		
	Handheld.Vibrate();
	
}

