//Define variables for script references
private var scoreScript : scoreTracker;
private var generatorScript : blockGenerator;
private var dragScript : dragRigidBodies;
private var gameOverScript : GUIgameOver;
private var smashScript : smashObjects;
private var countdownScript : GUIcountdown;
private var gameGuiScript : GUIgame;
private var gameWinScript : GUIwin;
private var pauseScript : GUIpause;
private var cameraFadeScript : cameraFade;
private var planeFadeScript : planeFade;


function Awake()
{
	PlayerPrefs.SetString("Level", Application.loadedLevelName);
}



function Start()
{
	scoreScript = GameObject.Find("levelScripts").GetComponent(scoreTracker);
	generatorScript = GameObject.Find("levelScripts").GetComponent(blockGenerator);
	dragScript = GameObject.Find("rigidBodyDragger").GetComponent(dragRigidBodies);
	smashScript = GameObject.Find("levelScripts").GetComponent(smashObjects);
	countdownScript = GameObject.Find("gameGUI").GetComponent(GUIcountdown);
	gameGuiScript = GameObject.Find("gameGUI").GetComponent(GUIgame);
	gameWinScript = GameObject.Find("gameGUI").GetComponent(GUIwin);
	gameOverScript = GameObject.Find("gameGUI").GetComponent(GUIgameOver);
	pauseScript = GameObject.Find("gameGUI").GetComponent(GUIpause);
	cameraFadeScript = Camera.main.GetComponent(cameraFade);
	planeFadeScript = GameObject.Find("fadePlane").GetComponent(planeFade);
	
}


//StartLevel() called by GUIcountdown.Start() after countdown finishes
function StartLevel()
{
	generatorScript.enabled = true;  //start generator
	scoreScript.enabled = true;  //start tracking score and couunting down score
	scoreScript.startCounting = true;
	dragScript.enabled = true;  //enable drag and drop ability
	gameGuiScript.buttonsEnabled = true;  //allow user to use gui buttons in GUIgame
}



function YouWin()
{
	generatorScript.enabled = false;
	scoreScript.enabled = false;
	scoreScript.startCounting = false;
	dragScript.enabled = false;
	gameGuiScript.buttonsEnabled = false;
	smashScript.SmashAll();
	PlayerPrefs.SetInt("TotalSmashed", PlayerPrefs.GetInt("TotalSmashed") + scoreScript.numberSmashed);
	
	cameraFadeScript.FadeOut(0, 1, 1.5, 0, .25);
	
	yield WaitForSeconds(.5);
	
	gameWinScript.enabled = true;
}



function GameOver()
{
	generatorScript.enabled = false;
	scoreScript.enabled = false;
	scoreScript.startCounting = false;
	dragScript.enabled = false;
	gameGuiScript.buttonsEnabled = false;
	smashScript.SmashAll();
	PlayerPrefs.SetInt("TotalSmashed", PlayerPrefs.GetInt("TotalSmashed") + scoreScript.numberSmashed);
	
	cameraFadeScript.FadeOut(0, 1, 1.5, 0, .25);
		
	yield WaitForSeconds(.75);

	gameOverScript.enabled = true;
}


function PauseGame()
{
	if (GameObject.Find("Audio") && GameObject.Find("Audio").GetComponent.<AudioSource>().isPlaying)
		GameObject.Find("Audio").GetComponent.<AudioSource>().Pause();
	
	Time.timeScale = 0.0;
	gameGuiScript.buttonsEnabled = false;
	pauseScript.enabled = true;
}


function UnpauseGame()
{
	if (GameObject.Find("Audio") && !GameObject.Find("Audio").GetComponent.<AudioSource>().isPlaying)
		GameObject.Find("Audio").GetComponent.<AudioSource>().Play();
		
	Time.timeScale = 1.0;
	gameGuiScript.buttonsEnabled = true;
	pauseScript.enabled = false;
}