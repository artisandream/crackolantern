var fadeSpeed : float = 2.0;
var loopSpeed : float = 0;
var delay : float = 0;

//Styles//
var startBackground : GUIStyle;
var startBackgroundHD : GUIStyle;

var resumeButton : GUIStyle;

private var fadeScript : cameraFade;
private var pumpkinSFXscript : eeSFX;
private var fadeDone : boolean;


function Awake()
{
	fadeScript = Camera.main.GetComponent(cameraFade);
}

function Update()
{
	fadeDone = fadeScript.fadeComplete;
}


function OnGUI () {
	GUI.depth = 0;

	var theDeviceWidth = Screen.width;
	
	if (theDeviceWidth < 700){
		if (GUI.Button (Rect (-96,-16,512,512), "", startBackground))
		{
			StartGame("menu");
		}
	} else {
		if (GUI.Button (Rect (-128,0,1024,1024), "", startBackgroundHD))
		{
			StartGame("menu");
		}
	}

}



function StartGame(level : String)
{
	fadeScript.fadeComplete = false;
	fadeScript.FadeOut(0, 1, fadeSpeed, loopSpeed, delay);
	
	while (!fadeScript.fadeComplete)
	{
		yield;
	}
	
	Application.LoadLevel(level);
}
