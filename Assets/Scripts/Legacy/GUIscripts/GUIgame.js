var alpha : float = 1.0;
var buttonsEnabled : boolean = true;

//***STYLES***//
var startBackground : GUIStyle;
var startBackgroundHD : GUIStyle;
var pauseButton : GUIStyle;
var muteButton : GUIStyle;
var muteOff : Texture2D;
var muteOn : Texture2D;
var theScoreBar : GUIStyle;
var theDamageBar : GUIStyle;
var theFontStyle : GUIStyle; 

private var meterWidth : float = 119.0;
private var mute : boolean = false;

//references to other scripts
private var scoreScript : scoreTracker;
private var levelScript : startEndLevel;



function Start()
{
	if (GameObject.Find("levelScripts"))
	{
		scoreScript = GameObject.Find("levelScripts").GetComponent(scoreTracker);
		levelScript = GameObject.Find("levelScripts").GetComponent(startEndLevel);
	}
	
	if (GameObject.Find("Audio") && GameObject.Find("Audio").GetComponent.<AudioSource>().volume == 0)
	{
		mute = true;
		muteButton.normal.background = muteOn;	
	}	
}



function OnGUI ()
{
	GUI.depth = 0;

	var scoreRatio : float = meterWidth / scoreScript.maxScore;
	var damageRatio : float = meterWidth / scoreScript.enemyHealth;
	
	
	var theDeviceWidth = Screen.width;
//	var thePauseDifference: int;
	
	if (theDeviceWidth > 700){
		GUI.Box (Rect (0,0,768,140), "", startBackgroundHD);
//		thePauseDifference = 730;
	} else {
		GUI.Box (Rect (0,0,320,480), "", startBackground);
//		//thePauseDifference = 284;
	}	
	

	if (GUI.Button (Rect (6,Screen.height - 37,32,32), "", pauseButton)) {
		if (buttonsEnabled)
		{
			levelScript.PauseGame();
		}
	}    


		
	if (GUI.Button (Rect (Screen.width - 37, Screen.height - 37, 32, 32), "", muteButton))
	{
		if (GameObject.Find("Audio") && GameObject.Find("Audio").GetComponent.<AudioSource>().isPlaying && !mute)
		{
			mute = true;
			muteButton.normal.background = muteOn;
			GameObject.Find("Audio").GetComponent.<AudioSource>().volume = 0;
		}
		else if (GameObject.Find("Audio") && GameObject.Find("Audio").GetComponent.<AudioSource>().isPlaying && mute)
		{
			mute = false;
			muteButton.normal.background = muteOff;
			GameObject.Find("Audio").GetComponent.<AudioSource>().volume = 1;	
		}			
	}
	
	 
	 if (scoreScript)
	{	 	
	 	GUI.Box ( Rect ( 71, 26, scoreRatio * scoreScript.score, 15 ), "", theScoreBar);
	 	GUI.Box ( Rect ( 192, 26, damageRatio * scoreScript.enemyDamage, 15 ), "", theDamageBar);

//      GUI.Label (Rect (5, 32, 31, 20), scoreScript.score.ToString("f1"), theFontStyle);	
	}
	else
	{	
	 	GUI.Box ( Rect ( 71, 26, meterWidth, 15 ), "", theScoreBar);
	 	GUI.Box ( Rect ( 192, 26, 0, 15 ), "", theDamageBar);

//	 	GUI.Label (Rect (5, 32, 31, 20), "0", theFontStyle);
	}
	
	

}


