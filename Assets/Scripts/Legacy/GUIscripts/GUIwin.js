var alignCenterSkin : GUISkin;
var nextLevelLabel : GUIStyle;
var continueButton : GUIStyle;
var menuButton : GUIStyle;
var winMenuButton : GUIStyle;
var theFontStyle : GUIStyle;
var nextLevelTexture : Texture2D;


var resumeTexture : Texture2D;
var resumeTextureHD : Texture2D;


var winCornerSmall : Texture2D;

var winEasySmall : Texture2D;
var winHardSmall : Texture2D;
var winImpassibleSmall : Texture2D;

var winAudio : AudioClip;

private var slideIn : boolean = true;

private var menuPositionY : float = -250;
private var sfx : sfxScript;


function Start()
{
	sfx = GameObject.Find("SFX").GetComponent(sfxScript);
	sfx.PlayPumpkinSFX(Random.Range(6, 9));
}



function Update()
{
	if (slideIn)
	{
		menuPositionY = Mathf.SmoothStep(menuPositionY, Screen.height / 2, .3);
	}
	else
	{
		menuPositionY = Mathf.SmoothStep(menuPositionY, -250, .3);
	}
}



function OnGUI()
{
	GUI.depth = -200;	
	GUI.skin = alignCenterSkin;
	var theDeviceWidth = Screen.width;
	
	
	if (Application.loadedLevel < Application.levelCount - 1)
	{
		GUI.DrawTexture (Rect (Screen.width / 2 - 98, menuPositionY - 250, 196, 236), nextLevelTexture);
		
		GUI.Label (Rect (Screen.width / 2 - 128, menuPositionY - 10, 256, 64), "", nextLevelLabel);
		
		if(GUI.Button (Rect (Screen.width / 2 - 90, menuPositionY + 70, 180, 50), "", continueButton)) 
		{
			LevelLoader(Application.loadedLevel + 1, .5);
		}
	}
	else
	{
		
		if (theDeviceWidth > 700){
					GUI.DrawTexture (Rect (-128, 0, 1024, 1024), resumeTextureHD);
				} else {
					GUI.DrawTexture (Rect (-96,-16,512,512), resumeTexture);
		}

		GUI.DrawTexture (Rect (Screen.width-Screen.width, Screen.height-80, 64, 128), winCornerSmall);
		
		

		switch(PlayerPrefs.GetString("Difficulty"))
		{
			case "Hard":
				GUI.DrawTexture (Rect ((Screen.width/2)-128, Screen.height/2, 128, 256), winEasySmall);
			break;
				
				
				
			case "Impassible":
				GUI.DrawTexture (Rect ((Screen.width/2)-128, Screen.height/2, 128, 256), winHardSmall);
			break;
				
				
				
			default:  //easy difficulty
				GUI.DrawTexture (Rect ((Screen.width/2)-128, Screen.height/2, 128, 256), winImpassibleSmall);
			break;
		} 
		
		
		
		if (theDeviceWidth > 700)
		{
			GUI.Label (Rect (48, Screen.height - 50, 70, 36), PlayerPrefs.GetInt("TotalSmashed").ToString(), theFontStyle);
		
			if(GUI.Button (Rect (Screen.width - 129, Screen.height - 50, 120, 36), "", winMenuButton)) 
			{
			//var menuIndex : int = 1;
			LevelLoader(1, .5); //return to main menu
    		}
    		
		} else {
			GUI.Label (Rect (48, menuPositionY + 188, 70, 36), PlayerPrefs.GetInt("TotalSmashed").ToString(), theFontStyle);
	
			if(GUI.Button (Rect (Screen.width - 129, menuPositionY + 189, 120, 36), "", winMenuButton)) 
    		{
			//var menuIndex : int = 1;
			LevelLoader(1, .5); //return to main menu
    		}
		}
		

	}	
}



function LevelLoader(level : int, loadDelay : float)
{
	
	menuPositionY = Screen.height / 2;
	slideIn = false;
	yield WaitForSeconds(loadDelay);
	Application.LoadLevel(level);
}



/*
function BlendAlpha(step : float)
{
	minAlpha = 0;
	maxAlpha = 1;
	
	if(alpha == minAlpha)
	{
		while (alpha < maxAlpha)
		{

			alpha += step;
			yield;
		}
		alpha = maxAlpha;
	}
	else
	{
		while (alpha > minAlpha)
		{
			alpha -= step;
			yield;
		}
		alpha = minAlpha;
	}
}
*/


