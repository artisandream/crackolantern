﻿using UnityEngine;
using System.Collections;

public class PumpkinStatesSprite : PumpkinStates {

	private SpriteRenderer thisSprite;
	
	public override void ChangeStage (int _i) {
		thisSprite = GetComponent<SpriteRenderer> ();
		thisSprite.sprite = 
			pumpkinController.pumpkinSprites [_i];
		print (pumpkinController.pumpkinSprites [_i]);
		print (_i);
	}
}
