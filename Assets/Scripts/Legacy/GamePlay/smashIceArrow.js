var iceParticles : GameObject;

private var sfx : sfxScript;


function Start()
{
	sfx = GameObject.Find("SFX").GetComponent(sfxScript);
}



function OnCollisionEnter ( other : Collision )
{
	if(other.gameObject.tag != "wall")
		Smash(gameObject);
		
}



function Smash(object : GameObject)
{
	//ensure an object has been passed
	if (!object)
		return;

	var particlePrefab : GameObject = iceParticles;
	var	smashSound : int = 5;  //need to set correct index
		
		
	if (!GameObject.Find("particleGroup"))
	{	
		particleGroup = new GameObject ("particleGroup");
	}
	
	
	var newParticles : GameObject = Instantiate (particlePrefab, object.transform.position + Vector3(0, 0, .25), Quaternion.identity);
	newParticles.transform.parent = GameObject.Find ("particleGroup").transform;

	//******PLAY AUDIO******//
	sfx.PlayPumpkinSFX(smashSound);
	
	Destroy(object);
}
