var ghost0 : GameObject;
var ghost1 : GameObject;
var ghost2 : GameObject;


function CreateGhost (ghostNum : int, spawnPosition : Vector3)
{
	var ghost : GameObject;
	switch (ghostNum)
	{
		case 0:
			ghost = ghost0;
			break;
		case 1:
			ghost = ghost1;
			break;
		case 2: 
			ghost = ghost2;
			break;
		default:
			ghost = ghost0;
			break;	
	}
	
	if (!GameObject.Find ( "ghostGroup" ))
		{	
			particleGroup = new GameObject ("ghostGroup");
		}
	var newGhost : GameObject = Instantiate (ghost, spawnPosition, Quaternion.identity);
	newGhost.transform.parent = GameObject.Find ("ghostGroup").transform;
}