var eeSounds : AudioClip[];

function PlayEasterEgg()
{	
	if (!GetComponent.<AudioSource>().isPlaying)
	{
		var randNum : int = Mathf.Round(Random.Range(0, eeSounds.Length));
	 	GetComponent.<AudioSource>().clip = eeSounds[randNum];
	 	GetComponent.<AudioSource>().volume = 2;
		GetComponent.<AudioSource>().Play();	
	}
}