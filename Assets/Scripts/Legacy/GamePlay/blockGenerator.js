//Start with this script off. Will turn on in levelAttribs.Start().
//Script will start when enabled

//Define variables
var objectToGenerate : GameObject;
var generatorWidth : float = .765;  //range between each wall
var delay : float = 2.0;  //delay between each instantiate
var numberOnScreen : int = 10;  //max number on screen at any given time
private var dropHeight : float = 4.5;  //where pumpkins are instantiated in y axis


//Variables to randomly determine type of pumpkin
var pumpkinFreq : int = 100; //in percentage
var fireFreq : int = 0; //in percentage
var iceFreq : int = 0; //in percentage
var bombFreq : int = 0; //in percentage
var fireTimeOut : float = 5.0;

private var frequencyMaxScale : int;  //larger scale reduces likelihood of generating certain objects

private var currentDepth : float = .01;  //used to offset z-depth preventing overlapping planes
private var blockGroup : GameObject;  //empty group used to hold all instantiated blocks


//Ensure script loads disabled
function Awake()
{	
	enabled = false;	
}



//When script starts, generator begins dropping objects
function Start()
{
	GenerateBlocks();	
}


//Used to randomize position and object tag
//Tag used by objectBehavior.Start() to assign proper attributes to object
function GenerateBlocks()
{

		
		
	while(enabled)
	{
		frequencyMaxScale = pumpkinFreq + fireFreq + iceFreq + bombFreq;
		
		//randominze tag based on defined variables
		var newTag : String = "pumpkin";
		var randNum : int = Mathf.CeilToInt(Random.Range(0.0, frequencyMaxScale));

		if(randNum > pumpkinFreq && randNum <= (pumpkinFreq + fireFreq))
		{
			newTag = "fire";
		}
		else if (randNum > (pumpkinFreq + fireFreq) && randNum<= (pumpkinFreq + fireFreq + iceFreq))
		{
			newTag = "ice";
		}
		else if (randNum > (pumpkinFreq + fireFreq + iceFreq))
		{
			newTag = "bomb";
		}

		
		//instantiate at random x-position and depth
		var newBlock : GameObject = Instantiate (objectToGenerate, Vector3(Random.Range(-generatorWidth, generatorWidth), dropHeight, currentDepth), Quaternion.identity);
		newBlock.tag = newTag;
		
		if (newTag == "fire")
		{
			var objectScript : objectBehavior = newBlock.GetComponent(objectBehavior);
			objectScript.fireTimer = fireTimeOut;
		}
			
		if (!blockGroup)
		{
			blockGroup = new GameObject ("blockGroup");	
		}
		
		newBlock.transform.parent = blockGroup.transform;

		//offset z variable to keep planes from lining up perfectly
		currentDepth = (Time.time % .19) - .1;

		//wait before looping back through this function
		yield WaitForSeconds (delay);
	}
}