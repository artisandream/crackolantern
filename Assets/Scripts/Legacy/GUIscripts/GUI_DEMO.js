
var gameDemo: boolean;
var demoEndTextureHD: Texture2D;
var demoEndTexture: Texture2D; 
var buyNowButton: GUIStyle;

var alignCenterSkin : GUISkin;
var menuButton : GUIStyle;
var winMenuButton : GUIStyle;
var theFontStyle : GUIStyle;

private var slideIn : boolean = true;

private var menuPositionY : float = -250;
private var sfx : sfxScript;


function Start()
{
	sfx = GameObject.Find("SFX").GetComponent(sfxScript);
	sfx.PlayPumpkinSFX(Random.Range(6, 9));
}



function Update()
{
	if (slideIn)
	{
		menuPositionY = Mathf.SmoothStep(menuPositionY, Screen.height / 2, .3);
	}
	else
	{
		menuPositionY = Mathf.SmoothStep(menuPositionY, -250, .3);
	}
}



function OnGUI()
{
	GUI.depth = -200;	
	GUI.skin = alignCenterSkin;
	var theDeviceWidth = Screen.width;
	
	
	if (gameDemo == false)
	{
		
	}
	else
	{
		switch(PlayerPrefs.GetString("Difficulty"))
		{
			case "Hard":
				if (theDeviceWidth > 700){
					GUI.DrawTexture (Rect (-128,0,1024,1024), demoEndTextureHD);
				} else {
					GUI.DrawTexture (Rect (Screen.width / 2 - 256, menuPositionY - 256, 512, 512), demoEndTexture);
				}
				break;
				
			case "Impassible":
				if (theDeviceWidth > 700){
					GUI.DrawTexture (Rect (-128,0,1024,1024), demoEndTextureHD);
				} else {
					GUI.DrawTexture (Rect (Screen.width / 2 - 256, menuPositionY - 256, 512, 512), demoEndTexture);
				}
				break;
				
			default:  //easy difficulty
				if (theDeviceWidth > 700){
					GUI.DrawTexture (Rect (-128,0,1024,1024), demoEndTextureHD);
				} else {
					GUI.DrawTexture (Rect (Screen.width / 2 - 256, menuPositionY - 256, 512, 512), demoEndTexture);
				}
				break;
		} 
		
		
		if (theDeviceWidth > 700)
		{
			GUI.Label (Rect (48, Screen.height - 50, 70, 36), PlayerPrefs.GetInt("TotalSmashed").ToString(), theFontStyle);
		
			if(GUI.Button (Rect (Screen.width - 129, Screen.height - 50, 120, 36), "", winMenuButton)) 
			{
			//var menuIndex : int = 1;
			LevelLoader(1, .5); //return to main menu
    		}
    		
    		if(GUI.Button (Rect (Screen.width - 259, Screen.height - 50, 120, 36), "", buyNowButton)) 
			{
			//var menuIndex : int = 1;
			LevelLoader(1, .5); //return to main menu
    		}
    		
    		
		} else {
			GUI.Label (Rect (48, menuPositionY + 188, 70, 36), PlayerPrefs.GetInt("TotalSmashed").ToString(), theFontStyle);
	
			if(GUI.Button (Rect (Screen.width - 129, menuPositionY + 189, 120, 36), "", winMenuButton)) 
    		{
			//var menuIndex : int = 1;
			LevelLoader(1, .5); //return to main menu
    		}
    		
    		if(GUI.Button (Rect (Screen.width - 129, menuPositionY + 226, 120, 36), "", buyNowButton)) 
    		{
			//var menuIndex : int = 1;
			LevelLoader(1, .5); //return to main menu
    		}
		}
		

	}	
}



function LevelLoader(level : int, loadDelay : float)
{
	
	menuPositionY = Screen.height / 2;
	slideIn = false;
	yield WaitForSeconds(loadDelay);
	Application.LoadLevel(level);
}



/*
function BlendAlpha(step : float)
{
	minAlpha = 0;
	maxAlpha = 1;
	
	if(alpha == minAlpha)
	{
		while (alpha < maxAlpha)
		{

			alpha += step;
			yield;
		}
		alpha = maxAlpha;
	}
	else
	{
		while (alpha > minAlpha)
		{
			alpha -= step;
			yield;
		}
		alpha = minAlpha;
	}
}
*/


