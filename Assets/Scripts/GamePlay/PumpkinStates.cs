﻿using UnityEngine;
using System.Collections;

public class PumpkinStates : MonoBehaviour {

	public PumpkinController pumpkinController;
	public StaticVars.AvaliableStates thisPumpkinsState;

	public virtual void ChangeStage (int _i) {

	}

	void SwitchState ( ){ 
		switch (thisPumpkinsState) {
		case StaticVars.AvaliableStates.Standard:
			ChangeStage (0);
			break;
		case StaticVars.AvaliableStates.Ice:
			ChangeStage (1);
			break;
		case StaticVars.AvaliableStates.Fire:
			ChangeStage (2);
			break;
		case StaticVars.AvaliableStates.Nuke:
			ChangeStage (3);
			break;
		}
	}

	void Start () {
		SwitchState ();
	}
}
