var resumeButton : GUIStyle;
var restartButton : GUIStyle;
var startBackground : GUIStyle;
var startBackgroundHD : GUIStyle;
var helpButton : GUIStyle;

private var levelScript : startEndLevel;
private var planeFadeScript : planeFade;
private var cameraFadeScript : cameraFade;
private var scoreScript : scoreTracker;


function Awake()
{
	enabled = false;
}



function Start()
{
	levelScript = GameObject.Find("levelScripts").GetComponent(startEndLevel);
	planeFadeScript = GameObject.Find("fadePlane").GetComponent(planeFade);
	cameraFadeScript = Camera.main.GetComponent(cameraFade);
	scoreScript = GameObject.Find("levelScripts").GetComponent(scoreTracker);
}



function OnGUI () {
	GUI.depth = -50;
	// Make a background box
	var theDeviceWidth = Screen.width;
	var menuPositionY : float = Screen.height*.5;
	if (theDeviceWidth > 700){
		GUI.Box (Rect (-128,0,1024,1024), "", startBackgroundHD);
	} else {
		GUI.Box (Rect (-96,-16,512,512), "", startBackground);
	}	
	
	if (GUI.Button (Rect (Screen.width / 2 - 90, menuPositionY + 70, 180, 50), " ", resumeButton)) {
		levelScript.UnpauseGame();
	}
	
	if (GUI.Button (Rect (Screen.width / 2 - 90, menuPositionY + 135, 180, 50), " ", restartButton)) {
		if (GameObject.Find("Audio") && !GameObject.Find("Audio").GetComponent.<AudioSource>().isPlaying)
			GameObject.Find("Audio").GetComponent.<AudioSource>().Play();
		
		
		PlayerPrefs.SetInt("TotalSmashed", PlayerPrefs.GetInt("TotalSmashed") + scoreScript.numberSmashed);
		
		var menuIndex : int = 1;
		LevelLoader(menuIndex, .5);
	}
}



function LevelLoader(level : int, loadDelay : float)
{
	Time.timeScale = 1.0;
	//planeFadeScript.FadeOut(0, 1, 5, 0, 0);
	cameraFadeScript.FadeOut(0, 1, 3, 0, 0);
	yield WaitForSeconds(loadDelay);
	Application.LoadLevel(level);
}
