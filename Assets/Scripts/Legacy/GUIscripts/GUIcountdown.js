//Define variables
var alignCenterSkin : GUISkin;
var smashTexture : Texture2D;
var levelTexture : Texture2D;
var numTextures : Texture2D[];


private var countdownTime : float = 6;

//Define references
private var fadeScript : cameraFade;
private var startEndScript : startEndLevel;
private var thisLevel : String = Application.loadedLevelName;
//private var thisLevelNum : String = thisLevel.Substring(thisLevel.length - 2);  //extract the last 2 digits representing the level#
private var firstDigit : int;
private var secondDigit : int;
private var slideIn : boolean = true;
private var menuPositionY : float = -50;
private var menuPositionX : float = Screen.width / 2 - 64;
private var levelPositionY : float = Screen.height + 100;


function Awake()
{
	//start level with startEndLevel.StartLevel() after timer ends
//	fadeScript = Camera.main.GetComponent(planeFade);
	
	if (GameObject.Find("levelScripts"))
	{
		startEndScript = GameObject.Find("levelScripts").GetComponent(startEndLevel);
	}

	firstDigit = parseInt(thisLevel.Substring(thisLevel.length - 2, 1));
	secondDigit = parseInt(thisLevel.Substring(thisLevel.length - 1, 1));
	
	//begin disabled to allow fade in to complete
	enabled = false;
}


//Begin countdown timer for GUI then 
function Start () 
{
	BeginCountdown();	
}



function Update()
{
	if (slideIn)
	{
		levelPositionY = Mathf.SmoothStep(levelPositionY, Screen.height - 92, .3);
		menuPositionY = Mathf.SmoothStep(menuPositionY, Screen.height / 2 - 85, .3);
	}
	else
	{
		levelPositionY = Mathf.SmoothStep(levelPositionY, Screen.height + 100, .1);
		menuPositionY = (Screen.height / 2 - 85) + Random.Range (-4, 4);
		menuPositionX = (Screen.width / 2 - 64) + Random.Range (-4, 4);
	}
}



//GUI displays 3.2.1. countadown at the beginning of each level
function OnGUI()
{
	GUI.depth = -25;
	GUI.skin = alignCenterSkin;
	var countdownTexture : Texture2D = null;
	
	GUI.Label (Rect (0, levelPositionY, 128, 64), levelTexture);
	GUI.Label (Rect (100, levelPositionY, 32, 64), numTextures[firstDigit]);
	GUI.Label (Rect (120, levelPositionY, 32, 64), numTextures[secondDigit]);
	
	if (countdownTime == 5)
	{
		countdownTexture = numTextures[3];
	}
	else if (countdownTime == 4)
	{
		countdownTexture = numTextures[2];
	}
	else if (countdownTime == 3)
	{
		countdownTexture = numTextures[1];	
	}
	else if (countdownTime < 3)
	{
		countdownTexture = smashTexture;	
	}
	
	if(countdownTime == 2)
	{
		levelPositionY = Screen.height - 92;
		slideIn = false;
	}
	
	
	
	if (countdownTime > 0)
	{
		GUI.Label (Rect (menuPositionX, menuPositionY, 128, 64), countdownTexture);
	}
	else 
	{
		//turn off GUI script
		enabled = false;	
	}
}




function BeginCountdown()
{
	
	while (countdownTime > 0)
	{	
		yield WaitForSeconds(.8);
		countdownTime -= 1;
		
		if (slideIn)
			menuPositionY = -50;
	}
	
	startEndScript.StartLevel();
}