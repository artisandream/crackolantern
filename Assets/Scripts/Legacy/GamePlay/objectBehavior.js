//pumpkin variables
var pumpkinMat : Material;  //materials for each pumpkin state
var crackMat : Material;
var fireMat : Material;
var bombMat : Material;
var iceMat : Material;
var crackParticles : GameObject;  //particle prefabs for each state/change of state
var burnParticles : GameObject;
var explodeParticles : GameObject;
var iceParticles : GameObject;
var fireTimer : float = 5.0;  //time in seconds that fire remains lit before changing to cracked
var beenSmashed : boolean = false;  //ensure smash and hazard kills can't coincide

private var myFireParticles : GameObject;  //used to track particles in case fire is changed to ice and need to kill fire particles
private var explodeTimer : float;  //limits the number of objects onscreen

//script references
private var generatorScript : blockGenerator;
private var smashScript : smashObjects;
private var sfx : sfxScript;
private var ghostExplodeScript : ghostExploder;


//set variable references
function Awake()
{
	generatorScript = GameObject.Find("levelScripts").GetComponent(blockGenerator);
	smashScript = GameObject.Find("levelScripts").GetComponent(smashObjects);
	sfx = GameObject.Find("SFX").GetComponent(sfxScript);
	ghostExplodeScript = GameObject.Find("levelScripts").GetComponent(ghostExploder);
}


//Tags defined at time of instantiation in levelScripts.blockGenerator
//This function assigns the proper material and changing state
function Start()
{
	if (tag == "pumpkin")
	{
		GetComponent.<Renderer>().material = pumpkinMat;
	}
	else if (tag == "cracked")
	{
		ChangeToCracked(false);	
	}
	else if (tag == "fire")
	{
		ChangeToFire();	
	}
	else if (tag == "ice")
	{
		ChangeToIce(false);	
	}
	else if (tag == "bomb")
	{
		ChangeToBomb();	
	}

	
	//timer set by taking the number on screen and multiplying by the generator delay time
	explodeTimer = generatorScript.delay * generatorScript.numberOnScreen;
	Explode(explodeTimer);
}


//function allows pumpkins at touching at rest to still change state
function OnCollisionStay (other : Collision) {
	//if object collides with an identically tagged object or a non-altering object, end the function
	if((tag == "pumpkin" && other.gameObject.tag == "fire") || 
		((tag == "pumpkin" || tag == "cracked" || tag == "fire") && (other.gameObject.tag == "ice" || other.gameObject.tag == "iceHazard")))
	{
		checkPumpkinState(other);		
	}
}


//Changes objects into appropriate state based on specific tag combinations
function OnCollisionEnter (other : Collision) {
	//if object collides with an identically tagged object or a non-altering object, end the function
	if(other.gameObject.tag == "pumpkin" || 
		other.gameObject.tag == "cracked" || 
		other.gameObject.tag == "bomb" || 
		other.gameObject.tag == "Untagged" || 
		tag == other.gameObject.tag ||
		tag == "rock" || 
		tag == "hazard" || 
		tag == "wall" || 
		tag == "Untagged")
	{
		return;	
	}
	else
	{
		
		checkPumpkinState(other);		
	}
}



function checkPumpkinState (other : Collision)
{
	var isRock : boolean = other.gameObject.tag == "rock";
	var isFire : boolean = other.gameObject.tag == "fire";
	var isIce : boolean = other.gameObject.tag == "ice";
	var isWall : boolean = other.gameObject.tag == "wall";
	var isHazard : boolean = other.gameObject.tag == "hazard";
	var isIceHazard : boolean = other.gameObject.tag == "iceHazard";
	var isEnemy : boolean = other.gameObject.tag == "enemy";

	
	//take out all other exeptions for collisions that have no effect
	/*
	if ((isWall && (tag == "pumpkin" || tag == "rock")) || (isRock && (tag == "cracked" || tag == "fire" || tag == "wall" || tag == "bomb")) || (isFire && (tag == "cracked" || tag == "bomb")))
	{
		return;	
	}
	*/

	if ((tag == "pumpkin" || tag == "ice") && isWall)
	{
		ChangeToCracked(true);			
	}
	else if (tag == "pumpkin" && isFire)
	{
		ChangeToFire();
	}
	else if ((tag == "pumpkin" || tag == "cracked" || tag == "fire") && (isIce || isIceHazard))
	{
		ChangeToIce(true);	
	}
	else if ((tag == "cracked" || tag == "fire") && isWall && !beenSmashed)
	{
		beenSmashed = true;
		smashScript.Smash(gameObject);
	}
	else if ((tag == "cracked" || tag == "fire" || tag == "bomb") && isEnemy && !beenSmashed)
	{
		beenSmashed = true;
		smashScript.SmashDamage(gameObject, other.gameObject);
	}
	else if (tag == "ice" && isWall)
	{
		ChangeToCracked(true);	
	}
	else if (tag == "bomb" && isWall)
	{
		smashScript.SmashAll();	
	}
	else if (isHazard && !beenSmashed)
	{
		beenSmashed = true;
		smashScript.SmashOnHazard(gameObject);
	}
}


//Change to cracked with optional particle creation
function ChangeToCracked(createParticles : boolean)
{
	var changeParticles : GameObject = crackParticles;
	if (tag == "ice")
		changeParticles = iceParticles;
	
	tag = "cracked";
	GetComponent.<Renderer>().material = crackMat;
	
	if (createParticles)
	{
		if (!GameObject.Find ( "particleGroup" ))
		{	
			particleGroup = new GameObject ("particleGroup");
		}
		
		var newParticles : GameObject = Instantiate (changeParticles, transform.position + Vector3(0, 0, .25), Quaternion.identity);
		newParticles.transform.parent = GameObject.Find ("particleGroup").transform;
	}
	
	sfx.PlayPumpkinSFX(1);
}


//Change to fire
function ChangeToFire()
{
	tag = "fire";
	GetComponent.<Renderer>().material = fireMat;
		
	var newParticles : GameObject = Instantiate (burnParticles, transform.position + Vector3(0, 0, .25), Quaternion.identity);
	newParticles.transform.parent = transform;
	Destroy(newParticles, fireTimer);
	
	myFireParticles = newParticles;
	
	sfx.PlayPumpkinSFX(2);
		
	yield WaitForSeconds (fireTimer);
	if (tag == "fire")
	{
		ChangeToCracked(true);
	}


}


//Change to ice with optional particle creation
function ChangeToIce(createParticles : boolean)
{
	if (tag == "fire")
	{
		Destroy(myFireParticles);	
	}
	
	tag = "ice";
	GetComponent.<Renderer>().material = iceMat;
	
	sfx.PlayPumpkinSFX(5);
	
	if (createParticles)
	{
		if (!GameObject.Find ( "particleGroup" ))
		{	
			particleGroup = new GameObject ("particleGroup");
		}
		
		var newParticles : GameObject = Instantiate (iceParticles, transform.position + Vector3(0, 0, .25), Quaternion.identity);
		newParticles.transform.parent = GameObject.Find ("particleGroup").transform;
	}
}



//Change to bomb
function ChangeToBomb()
{
	tag = "bomb";
	GetComponent.<Renderer>().material = bombMat;
}


function Explode(timer : float)
{
	var currentTime : float = Time.time;
	yield WaitForSeconds (timer);
	
	var radius : float = 1.0;
	var power : float = 750.0;

	// Applies an explosion force to all nearby rigidbodies
	var explosionPos = transform.position;
	var colliders : Collider[] = Physics.OverlapSphere (explosionPos, radius);
	
	for (var hit in colliders) {
		
		if (!hit || GetComponent.<Rigidbody>() == hit.GetComponent.<Rigidbody>())
		{
			continue;
		}
		
		if (hit.GetComponent.<Rigidbody>() && hit.GetComponent.<Rigidbody>().isKinematic == false )
		{
			hit.GetComponent.<Rigidbody>().AddExplosionForce(power, explosionPos, radius, 1.5);
		}
	}

	if (!GameObject.Find ( "particleGroup" ))
		{	
				particleGroup = new GameObject ("particleGroup");
		}
	
		var newParticles : GameObject = Instantiate (explodeParticles, transform.position + Vector3(0, 0, .25), Quaternion.identity);
		newParticles.transform.parent = GameObject.Find ("particleGroup").transform;
		
	//generate ghost
	ghostExplodeScript.CreateGhost(0, transform.position);
	
	Destroy(gameObject);
	
}