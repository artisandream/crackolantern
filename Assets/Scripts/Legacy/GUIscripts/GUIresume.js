var theNumOfPumks : int = 105;
var theNumOfLevels : int = 12;

var fadeSpeed : float = 2.0;
var loopSpeed : float = 0;
var delay : float = 0;

//**STYLES**//
var splashTexture : Texture2D;
var startBackground : GUIStyle;
var resumeButton : GUIStyle;
var newGameButton : GUIStyle;
var easterEggButton : GUIStyle;


private var fadeScript : cameraFade;
private var pumpkinSFXscript : eeSFX;

private var fadeDone : boolean;



function Awake()
{
	fadeScript = Camera.main.GetComponent(cameraFade);
	pumpkinSFXscript = GameObject.Find("pumpkinSFX").GetComponent(eeSFX);
}


function Update()
{
	fadeDone = fadeScript.fadeComplete;
}


function OnGUI () {
	GUI.depth = 0;
	
	// Make a background box
	GUI.Box (Rect (-96,-16,512,512), "", startBackground);
	
	if (GUI.Button (Rect (38, 90, 151, 135), "", easterEggButton))
	{
		if(fadeDone)
		{
			pumpkinSFXscript.PlayEasterEgg();
		}	
	}
	
	if (GUI.Button (Rect (100, 280, 180, 50), "", resumeButton)) {
		if(fadeDone)
		{
			//****insert last level and difficulty from prefs****////
			StartGame("level1");
		}
	}
	if (GUI.Button (Rect (100, 340, 180, 50), "", newGameButton)) {
		if(fadeDone)
		{
			StartGame("menu");
		}
	}
}



function StartGame(level : String)
{
	fadeScript.fadeComplete = false;
	fadeScript.FadeOut(0, 1, fadeSpeed, loopSpeed, delay);
	
	while (!fadeScript.fadeComplete)
	{
		yield;
	}
	
	Application.LoadLevel(level);
}